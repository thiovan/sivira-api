<?php
include 'config.php';
include 'authentication.php';

//check if request not empty
if (!empty($_SERVER['HTTP_AUTH'])) {

	//get request value
	$header = $_SERVER['HTTP_AUTH'];

	//check auth token
	if (check_auth($header, $AUTH['TOKEN'])) {

		$sql = "SELECT * FROM berita ORDER BY upload_date DESC LIMIT 10";
		$result = mysqli_query($conn, $sql);

		if (mysqli_num_rows($result) > 0){

			$json = array();
			while ($row = mysqli_fetch_assoc($result)) {

				$row_array['judul'] = $row['judul'];
				$row_array['banner'] = $row['thumbnail'];
				$row_array['url'] = $row['url'];
				if (($row['thumbnail'] != "no thumbnail") && (sizeof($json) < 5)) {
					array_push($json,$row_array);
				}
			}
		} else {

			$json['success'] = 0;
			$json['message'] = 'Banner Tidak Ditemukan';
		}
	} else {

		$json['success'] = 0;
		$json['message'] = 'Authentication Token Mismatch';
	}

	echo json_encode(array('data' => $json));
	
}
?>
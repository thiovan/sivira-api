<?php

function sendNotification($key, $target_id, $target_topic, $title, $judul_pengumuman, $isi_pengumuman)
{
	#API access key from Google API's Console
	define( 'FIREBASE_API_KEY', $key );
	#prep the bundle
	// $msg = array
	// (
	// 	'body'  => $message,
	// 	'title' => $title,
	// 	'data' => 'Coba data',
	// 	'icon'  => $icon,
	// 	'sound' => 'mySound'/*Default sound*/
	// );

	$data = array
	(
		'title' => $title,
	  	'judul_pengumuman' => $judul_pengumuman,
	  	'isi_pengumuman' => $isi_pengumuman
	);

	if (empty($target_id)) {
		$fields = array
		(
			'to'        => '/topics/' . $target_topic,
			//'notification'  => $msg,
			'data' => $data
		);
	} else {
		$fields = array
		(
			'to'        => $target_id,
			//'notification'  => $msg,
			'data' => $data
		);
	}

	$headers = array
	(
		'Authorization: key=' . FIREBASE_API_KEY,
		'Content-Type: application/json'
	);
 
	$ch = curl_init();
	curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
	curl_setopt( $ch,CURLOPT_POST, true );
	curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
	curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
	curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
	curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
	$result = curl_exec($ch );
	curl_close( $ch );

	return $result;
}

?>
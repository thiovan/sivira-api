<?php
include 'config.php';

$data = array(
	'latestVersion' => $APP['LATEST_VERSION'],
	'latestVersionCode' => $APP['LATEST_VERSION_CODE'],
	'url' => $APP['URL'],
	'releaseNotes' => $APP['CHANGELOG']
	);

echo json_encode($data);
?>
<?php
include 'config.php';
include 'authentication.php';

//check if request not empty
if (!empty($_SERVER['HTTP_AUTH']) && !empty($_FILES["file"]["name"]) && !empty($_POST['nomor_induk'])) {

	//get request value
	$header = $_SERVER['HTTP_AUTH'];
	$image_file = basename($_FILES["file"]["name"]);
	$nomor_induk = str_replace('"', '', $_POST['nomor_induk']) ;
	$image_extension = explode(".", $_FILES["file"]["name"]);
	$image_file = $nomor_induk.'.'.end($image_extension);

	//check auth token
	if (check_auth($header, $AUTH['TOKEN'])) {

		$check = getimagesize($_FILES["file"]["tmp_name"]);
		if ($check !== false) {

			if(move_uploaded_file($_FILES['file']['tmp_name'], $DIR['USER_IMAGE_UPLOAD'] . $image_file)) {

			    $sql = "UPDATE user SET foto='$image_file' WHERE nomor_induk='$nomor_induk'";
				$result = mysqli_query($conn, $sql);
				if (mysqli_query($conn, $sql)) {

					$json['success'] = 1;
					$json['message'] = $DIR['USER_IMAGE'] . $image_file;
				} else {

				    $json['success'] = 0;
					$json['message'] = 'Foto Gagal Diubah';
				}
			} else {

			    $json['success'] = 0;
				$json['message'] = 'Foto Gagal Di Upload, Mohon Coba Lagi';
			}
		} else {

			$json['success'] = 0;
			$json['message'] = 'Not Image File';
		}

		
	} else {

		$json['success'] = 0;
		$json['message'] = 'Authentication Token Mismatch';
	}

	echo json_encode($json);
	
}

?>
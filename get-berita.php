<?php
include 'config.php';
include 'authentication.php';

//check if request not empty
if (!empty($_SERVER['HTTP_AUTH'])) {

	//get request value
	$header = $_SERVER['HTTP_AUTH'];

	//check auth token
	if (check_auth($header, $AUTH['TOKEN'])) {

		$sql = "SELECT * FROM berita ORDER BY upload_date DESC LIMIT 12";
		$result = mysqli_query($conn, $sql);

		if (mysqli_num_rows($result) > 0){

			$json = array();
			while ($row = mysqli_fetch_assoc($result)) {
				$row_array['judul'] = $row['judul'];
				$row_array['thumbnail'] = $row['thumbnail'];
				$row_array['isi'] = $row['isi'];
				$row_array['sumber'] = $row['sumber'];
				$row_array['url'] = $row['url'];
				$date_formated = date_create($row['upload_date']);
				$row_array['upload_date'] = date_format($date_formated,"d/m/Y");
				array_push($json,$row_array);
			}
		} else {

			$json['success'] = 0;
			$json['message'] = 'Berita Tidak Ditemukan';
		}
	} else {

		$json['success'] = 0;
		$json['message'] = 'Authentication Token Mismatch';
	}

	echo json_encode(array('data' => $json));
	
}
?>
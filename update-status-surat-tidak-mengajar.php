<?php
include 'config.php';
include 'authentication.php';
include 'firebase.php';

//check if request not empty
if (!empty($_SERVER['HTTP_AUTH']) && !empty($_POST['id_surat'])) {

	//get request value
	$header = $_SERVER['HTTP_AUTH'];
	$id_surat = $_POST['id_surat'];
	$status = $_POST['status'];

	//check auth token
	if (check_auth($header, $AUTH['TOKEN'])) {

		$sql = "UPDATE surat_tidak_mengajar SET status='$status' WHERE id_surat='$id_surat'";
		$result = mysqli_query($conn, $sql);

		if (mysqli_query($conn, $sql)) {

				$sql = "SELECT token FROM firebase 
						INNER JOIN surat_tidak_mengajar ON firebase.id_user = surat_tidak_mengajar.pengusul
						WHERE surat_tidak_mengajar.id_surat='$id_surat' LIMIT 1";
				$result = mysqli_query($conn, $sql);
				$row = mysqli_fetch_assoc($result);
				$key = $AUTH['FIREBASE_API_KEY'];
				$targetNotifikasi = $row['token'];

			if ($status == '2') {
				
				$title = "Surat Tidak Bisa Mengajar";
				$judul_stbm = "Permintaan Konfirmasi Disetujui";
				$isi_stbm = "Silahkan buka aplikasi sivira untuk cetak surat";
				sendNotification($key, $targetNotifikasi, "", $title, $judul_stbm, $isi_stbm);
			} else if ($status == '0') {

				$title = "Surat Tidak Bisa Mengajar";
				$judul_stbm = "Permintaan Konfirmasi Ditolak";
				$isi_stbm = "Silahkan untuk memilih dosen pengganti lainnya";
				sendNotification($key, $targetNotifikasi, "", $title, $judul_stbm, $isi_stbm);
			}

			$json['success'] = 1;
			$json['message'] = 'Status Surat Berhasil Di Update';
		} else {

		    $json['success'] = 0;
			$json['message'] = 'Status Surat Gagal Di Update, Mohon Coba Lagi';
		}
	} else {

		$json['success'] = 0;
		$json['message'] = 'Authentication Token Mismatch';
	}

	echo json_encode($json);
	
}
?>
-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 12 Agu 2018 pada 23.50
-- Versi Server: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sivira`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `confirm_password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `username`, `password`, `confirm_password`) VALUES
(1, 'admin1', 'admin2', 'admin2');

-- --------------------------------------------------------

--
-- Struktur dari tabel `berita`
--

CREATE TABLE `berita` (
  `id_berita` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `isi` text NOT NULL,
  `sumber` varchar(50) NOT NULL,
  `url` varchar(255) NOT NULL,
  `upload_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `berita`
--

INSERT INTO `berita` (`id_berita`, `judul`, `thumbnail`, `isi`, `sumber`, `url`, `upload_date`) VALUES
(1, 'Dies Natalis Ke-36, Ganjar Harapkan Polines Jadi Pionir Teknologi Tepat Guna', 'https://www.lpmdimensi.com/wp-content/uploads/2018/08/IMG_2953-520x245.jpg', 'Polines, Dimensi (6/08) â€“ Gubernur Jawa Tengah, Ganjar Pranowo, memberikan sambutan dalam Rapat Senat Terbuka di Ruang Serba Guna (RSG) Politeknik Negeri Semarang (Polines) pada Senin (6/08), ia berharap agar Polines ke depannya dapat', 'LPM Dimensi', 'https://www.lpmdimensi.com/?p=2100', '2018-08-06 11:18:16'),
(2, 'Kelas Kerjasama Me, Pt. Maj Siapkan Bantuan Dan Beasiswa', 'https://www.lpmdimensi.com/wp-content/uploads/2018/07/IMG-20180717-WA0027-520x245.jpg', 'Polines, Dimensi (17/7) â€“ Politeknik Negeri Semarang (Polines) membuka jalur kelas kerjasama dengan PT Mekar Armada Jaya (MAJ) untuk Program Studi (Prodi) D3 Teknik Mesin (ME) pada tahun ajaran 2018/2019 mendatang. Hubungan kerjasama terkait', 'LPM Dimensi', 'https://www.lpmdimensi.com/?p=2077', '2018-07-17 09:28:06'),
(3, 'Indonesia Peringkat Kedua Penghasil Sampah Terbesar Dunia, Bagaimana Peran Pemuda?', 'https://www.lpmdimensi.com/wp-content/uploads/2018/07/IMG-20180708-WA00381-520x245.jpg', 'Polines, Dimensi (8/7) &#8211; Sampah menjadi momok permasalahan lingkungan di mana-mana, bahkan hampir di seluruh dunia. Berdasarkan data Jambeck (2015), Indonesia berada di peringkat kedua dunia penghasil sampah plastik ke laut yang mencapai 187,2', 'LPM Dimensi', 'https://www.lpmdimensi.com/?p=2063', '2018-07-09 23:46:27'),
(4, 'Realisasikan Tri Dharma Perguruan Tinggi Melalui Pkm-m', 'https://www.lpmdimensi.com/wp-content/uploads/2018/07/IMG-20180520-WA0025-520x245.jpg', '&nbsp; Polines, DIMENSI (02/07) &#8211; PKM (Program Kreativitas Mahasiswa) merupakan kegiatan yang diselenggarakan setiap tahun oleh Diltilabmas (Direktorat Penelitian dan Pengabdian kepada Masyarakat) Ditjen Dikti. Salah satunya adalah PKM-M yang merupakan wujud implementasi dari', 'LPM Dimensi', 'https://www.lpmdimensi.com/?p=2053', '2018-07-03 01:00:36'),
(5, 'Gelora Mahasiswa Bidikmisi, Ajak Mahasiswa Berprestasi', 'https://www.lpmdimensi.com/wp-content/uploads/2018/07/IMG_3609-1-520x245.jpg', '&nbsp; Polines, DIMENSI (01/07) â€“ Pada tahun ini Keluarga Besar Mahasiswa Bidikmisi Politeknik Negeri Semarang (Kamadiksi Polines) untuk pertama kalinya mengadakan acara Gelora Mahasiswa Bidikmisi (GMB). Acara ini dilaksanakan pada Sabtu (30/6) sampai Minggu', 'LPM Dimensi', 'https://www.lpmdimensi.com/?p=2042', '2018-07-02 07:22:56'),
(6, 'Jalur Fast Track, Antara Efektif Efisien Dan Mahal', 'https://www.lpmdimensi.com/wp-content/uploads/2018/06/foto-pendukung-520x245.jpg', 'Polines, Dimensi (21/5) â€“ Tahun ini Politeknik Negeri Semarang (Polines) membuka jalur baru dalam seleksi penerimaan mahasiswa baru yaitu jalur fast track. Jalur fast track merupakan jalur kerjasama antara Polines dengan Management and Science', 'LPM Dimensi', 'https://www.lpmdimensi.com/?p=1997', '2018-06-06 09:00:55'),
(7, 'Mengkaji Rencana Pilkahim Serentak Dengan Pansus', 'https://www.lpmdimensi.com/wp-content/uploads/2018/05/DSC_0827-520x245.jpg', 'Polines, DIMENSI (28/05) â€“ Ditetapkannya rekomendasi Produk Legislasi Kampus oleh forum Kongres Mahasiswa (KM), maka berakhir sudah KM 2018. Kongres mahasiswa 2018 berakhir pada hari Minggu (27/05) sekitar pukul 20.30 WIB. Terdapat beberapa hal', 'LPM Dimensi', 'https://www.lpmdimensi.com/?p=1989', '2018-05-28 03:24:42'),
(8, 'Terpilihnya Ketua Bpm Baru Sebelum Kongres Mahasiswa 2018', 'https://www.lpmdimensi.com/wp-content/uploads/2018/05/IMG-20180526-WA0025-520x245.jpg', 'Polines, DIMENSI (27/05) &#8211; Pemilihan ketua Badan Perwakilan Mahasiswa (BPM) Â periode 2018/2019 dilakukan dengan mekanisme yang ditetapkan secara internal oleh anggota BPM Polines terpilih periode 2018/2019. Mekanisme tahun ini sedikit berbeda dari tahun sebelumnya,', 'LPM Dimensi', 'https://www.lpmdimensi.com/?p=1986', '2018-05-28 02:24:55'),
(9, 'Tujuh Poin Sorotan Dalam Sidang Istimewa 2018', 'https://www.lpmdimensi.com/wp-content/uploads/2018/05/IMG-20180526-WA0020-520x245.jpg', 'Polines, DIMENSI (27/5) â€“ Kongres Mahasiswa (KM) merupakan forum pengambilan keputusan tertinggi dalam Keluarga Besar Mahasiswa (KBM) Politeknik Negeri Semarang (Polines). Sesuai agenda yang telah direncanakan dalam kegiatan Pra KM pada hari Kamis (24/5),', 'LPM Dimensi', 'https://www.lpmdimensi.com/?p=1977', '2018-05-27 03:03:48'),
(10, 'Berakhirnya Masa Reses Ii Bpm, Sudahkah Aspirasi Terealisasi?', 'https://www.lpmdimensi.com/wp-content/uploads/2018/05/IMG-20180521-WA0027-520x245.jpg', 'Polines, DIMENSI (21/05) &#8211;Â  Masa reses Badan Perwakilan Mahasiswa (BPM) Politeknik Negeri Semarang (Polines) merupakan implementasi dari salah satu tugas BPM Polines, yaitu menjaring serta menampung aspirasi mahasiswa maupun ormawa. Masa reses dicetuskan pada', 'LPM Dimensi', 'https://www.lpmdimensi.com/?p=1971', '2018-05-22 01:13:01'),
(11, 'Polines Rintis Sebagai Pusat Inovasi Teknologi Dan Bisnis Yang Terintegrasi', 'https://www.polines.ac.id/id/images/FotoArtikel/intro_225x150/20180810_225x150.jpg', 'Politeknik Negeri Semarang (Polines) mengadakan Rapat Senat Terbuka dengan agenda upacara Dies Natalis ke-36 yang berlangsung di Ruang Serba Guna (RSG), Kampus Polines, Tembalang, Senin (6/8).', 'Polines.ac.id', 'https://www.polines.ac.id/id/index.php/berita/826-polines-rintis-sebagai-pusat-inovasi-teknologi-dan-bisnis-yang-terintegrasi', '2018-08-10 07:46:45'),
(12, 'Direktur Lepas 10 Calon Jamaah Haji ', 'https://www.polines.ac.id/id/images/FotoArtikel/intro_225x150/20180809-calon-haji-int.jpg', 'Direktur Politeknik Negeri Semarang (Polines) Ir. Supriyadi, MT melepasÂ 10 pegawai yang akan menunaikan ibadah haji tahun 2018. Pelepasan berlangsung di Aula Masjid Darul Hikmah, Kampus Polines, Tembalang, Senin, (30/7).', 'Polines.ac.id', 'https://www.polines.ac.id/id/index.php/berita/825-direktur-polines-lepas-10-calon-jamaah-haji', '2018-08-05 14:11:48'),
(13, 'Tim Polines Raih Juara 2 Dan 3 Ajang Soegijapranata Debating Championship 2018', 'https://www.polines.ac.id/id/images/FotoArtikel/intro_225x150/20180801_Intro.png', 'Tim Politeknik Negeri Semarang (Polines) berhasil meraih juara kedua dan juara ketiga lomba debat bahasa Inggris Soegijapranata Debating Championship (SDC).', 'Polines.ac.id', 'https://www.polines.ac.id/id/index.php/berita/821-tim-polines-juara-lomba-debat-di-unika-soegijapranata', '2018-08-01 14:38:09'),
(14, 'Polines Bantu Pengrajin Bambu Di Salatiga Guna Meningkatkan Daya Saing', 'https://www.polines.ac.id/id/images/FotoArtikel/intro_225x150/20180809_225x150.jpg', 'Selama ini banyak Usaha Mikro Kecil dan Menengah (UMKM) yang menjalankan bisnisnya dengan bermodalkan peralatan yang sederhana. Padahal dengan tambahan teknologi, UMKM dapat berkembang dan mampu bersaing.', 'Polines.ac.id', 'https://www.polines.ac.id/id/index.php/berita/824-polines-bantu-pengrajin-bambu-di-salatiga-guna-meningkatkan-daya-saing', '2018-08-09 10:38:26'),
(15, 'Polines Gelar Tes Tertulis Ujian Mandiri Program Kerjasama Di Kab. Kepulauan Mentawai', 'https://www.polines.ac.id/id/images/FotoArtikel/intro_225x150/20180801_mentawai.jpg', 'Sebagai realisasi dari kesepakatan perjanjian kerjasama antara Politeknik Negeri Semarang (Polines) dan Pemerintah Kabupaten Kepulauan Mentawai, Sumatera Barat, dilaksanakan kegiatan Tes Tertulis Ujian Mandiri Program Kerjasama, yang berlangsung di kampus PDD Akademi Komunitas Negeri Mentawai (AKNM), Selasa, 31 Juli 2018.', 'Polines.ac.id', 'https://www.polines.ac.id/id/index.php/berita/822-polines-gelar-tes-tertulis-ujian-mandiri-program-kerjasama-di-kab-kepulauan-mentawai', '2018-08-01 14:49:44'),
(16, 'Tiga Dosen Prodi Perbankan Syariah Polines Ikuti Summer School Di Durham University Business School London, Inggris', 'https://www.polines.ac.id/id/images/FotoArtikel/intro_225x150/20180727-islamic-financy-intro.jpg', 'Sebanyak 3 (tiga (dosen) program studi Perbankan Syariah, Politeknik Negeri Semarang (Polines) yakni Vita Arumsari, SST., M.Sc., Atif Windawati, SST., MBA. dan Suryani Sri Lestari, SE. M.Bus mengikuti kegiatan Summer SchoolÂ di Durham University Business School.', 'Polines.ac.id', 'https://www.polines.ac.id/id/index.php/berita/820-tiga-dosen-prodi-perbankan-syariah-polines-ikuti-summer-school-di-durham-university-business-school-london-inggris', '2018-07-27 13:45:10');

-- --------------------------------------------------------

--
-- Struktur dari tabel `feedback`
--

CREATE TABLE `feedback` (
  `id_feedback` int(11) NOT NULL,
  `pengirim` int(11) NOT NULL,
  `feedback` text COLLATE utf8_unicode_ci NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `feedback`
--

INSERT INTO `feedback` (`id_feedback`, `pengirim`, `feedback`, `tanggal`) VALUES
(1, 2, 'uji pertama\n\n\nHardware Detail:\nSERIAL: 58d1a5037d32\nMODEL: wt88047\nID: LMY47V\nMANUFACTURE: Xiaomi\nBRAND: Xiaomi\nTYPE: user\nUSER: builder\nBASE: 1\nINCREMENTAL V8.1.3.0.LHJMIDI\nSDK  22\nBOARD: msm8916\nHOST c3-miui-ota-bd39.bj\nFINGERPRINT: Xiaomi/wt88047/wt88047:5.1.1/LMY47V/V8.1.3.0.LHJMIDI:user/release-keys\nVERSION CODE: 5.1.1', '2018-05-27 07:37:31'),
(2, 32, 'ganti toolbartitle berita jadi detail berita, foto header navigasi di intent ke profil, load gmbar profil lama, kompress gambar\r\n\r\n\r\nHardware Detail:\r\nSERIAL: H2AXGF01P577ASJ\r\nMODEL: ASUS_X008DA\r\nID: NRD90M\r\nMANUFACTURE: asus\r\nBRAND: asus\r\nTYPE: user\r\nUSER: miaoyulu\r\nBASE: 1\r\nINCREMENTAL ID_Phone-14.11.1801.98-20180131\r\nSDK  24\r\nBOARD: MT6737\r\nHOST SOFT30-14\r\nFINGERPRINT: asus/ID_Phone/ASUS_X008_1:7.0/NRD90M/ID_Phone-14.11.1801.98-20180131:user/release-keys\r\nVERSION CODE: 7.0', '2018-07-06 02:20:02'),
(3, 32, 'apanih\n\n\nHardware Detail:\nSERIAL: H2AXGF01P577ASJ\nMODEL: ASUS_X008DA\nID: NRD90M\nMANUFACTURE: asus\nBRAND: asus\nTYPE: user\nUSER: miaoyulu\nBASE: 1\nINCREMENTAL ID_Phone-14.11.1801.98-20180131\nSDK  24\nBOARD: MT6737\nHOST SOFT30-14\nFINGERPRINT: asus/ID_Phone/ASUS_X008_1:7.0/NRD90M/ID_Phone-14.11.1801.98-20180131:user/release-keys\nVERSION CODE: 7.0', '2018-07-06 02:19:56'),
(17, 39, 'Fitur nya cukup lengkap, tapi mungkin penataannya lebih dirapikan lagi \n\n\nHardware Detail:\nSERIAL: HNC0279D\nMODEL: Lenovo A2010-a\nID: LMY47D\nMANUFACTURE: LENOVO\nBRAND: Lenovo\nTYPE: user\nUSER: queen\nBASE: 1\nINCREMENTAL A2010-a-t_S264_170110\nSDK  22\nBOARD: A2010-a\nHOST scmbuild\nFINGERPRINT: Lenovo/A2010-a/A2010-a:5.1/LMY47D/A2010-a-t_S264_170110.170111:user/release-keys\nVERSION CODE: 5.1', '2018-08-04 10:08:54'),
(6, 32, 'enak, wes meh dadi ðŸ˜‚', '2018-07-06 02:20:20'),
(8, 32, 'keren update meneh  ðŸ¤£', '2018-07-06 02:19:47'),
(9, 1, 'Coba gan', '2018-06-24 23:30:37'),
(15, 24, '\n\n\nHardware Detail:\nSERIAL: 0123456789ABCDEF\nMODEL: Alcatel_7049D\nID: LMY47D\nMANUFACTURE: TCL\nBRAND: TCL\nTYPE: user\nUSER: jenkins\nBASE: 1\nINCREMENTAL 1444319286\nSDK  22\nBOARD: unknown\nHOST jenkins-214\nFINGERPRINT: TCL/Alcatel_7049D/Lion-5:5.1/LMY47D/1444319286:user/release-keys\nVERSION CODE: 5.1', '2018-08-04 03:43:38'),
(16, 29, 'mantap\n\n\nHardware Detail:\nSERIAL: 4014f2ed7d94\nMODEL: Redmi 5A\nID: N2G47H\nMANUFACTURE: Xiaomi\nBRAND: Xiaomi\nTYPE: user\nUSER: builder\nBASE: 1\nINCREMENTAL V9.6.2.0.NCKMIFD\nSDK  25\nBOARD: QC_Reference_Phone\nHOST mi-server\nFINGERPRINT: Xiaomi/riva/riva:7.1.2/N2G47H/V9.6.2.0.NCKMIFD:user/release-keys\nVERSION CODE: 7.1.2', '2018-08-04 09:52:19'),
(12, 44, 'Laporan Jadwal Tidak Sesuai\nKelas: IK-2A\nMata Kuliah: Bahasa Indonesia\n\nSaran Perubahan\nJam Mulai: 08:30\nJam Selesai: 10:20', '2018-07-28 06:57:19'),
(13, 24, 'Laporan Jadwal Tidak Sesuai\nKelas: \nMata Kuliah: Tugas Akhir\n\nSaran Perubahan\nJam Mulai: 07:30\nJam Selesai: 15:30', '2018-07-31 22:13:53'),
(14, 24, 'Laporan Jadwal Tidak Sesuai\nKelas: \nMata Kuliah: Sistem Basis Data Dasar (TP)\n\nSaran Perubahan\nJam Mulai: 07:00\nJam Selesai: 11:50', '2018-07-31 22:14:43'),
(18, 33, 'Laporan Jadwal Tidak Sesuai\nKelas: IK-3A\nMata Kuliah: Sistem Manajemen Mutu\n\nSaran Perubahan\nJam Mulai: 07.00\nJam Selesai: 14:00', '2018-08-04 12:25:50'),
(19, 33, 'Laporan Jadwal Tidak Sesuai\nKelas: IK-3A\nMata Kuliah: Sistem Manajemen Mutu\n\nSaran Perubahan\nJam Mulai: 07.00\nJam Selesai: 14:00', '2018-08-04 12:26:06'),
(20, 33, 'Laporan Jadwal Tidak Sesuai\nKelas: IK-3A\nMata Kuliah: Sistem Manajemen Mutu\n\nSaran Perubahan\nJam Mulai: 07.00\nJam Selesai: 14:00', '2018-08-04 12:26:20'),
(21, 33, 'lonceng notif belum bisa di klik', '2018-08-04 12:31:44'),
(22, 33, 'aplikasinya keren', '2018-08-04 12:31:53'),
(23, 33, 'hestimr\n\n\nHardware Detail:\nSERIAL: 5200174aeea47419\nMODEL: SM-J701F\nID: NRD90M\nMANUFACTURE: samsung\nBRAND: samsung\nTYPE: user\nUSER: dpi\nBASE: 1\nINCREMENTAL J701FXXU4ARB3\nSDK  24\nBOARD: universal7870\nHOST SWDD6619\nFINGERPRINT: samsung/j7veltedx/j7velte:7.0/NRD90M/J701FXXU4ARB3:user/release-keys\nVERSION CODE: 7.0', '2018-08-04 12:53:32'),
(24, 33, 'lonceng notifnya blm bisa di klik\n\n\nHardware Detail:\nSERIAL: 5200174aeea47419\nMODEL: SM-J701F\nID: NRD90M\nMANUFACTURE: samsung\nBRAND: samsung\nTYPE: user\nUSER: dpi\nBASE: 1\nINCREMENTAL J701FXXU4ARB3\nSDK  24\nBOARD: universal7870\nHOST SWDD6619\nFINGERPRINT: samsung/j7veltedx/j7velte:7.0/NRD90M/J701FXXU4ARB3:user/release-keys\nVERSION CODE: 7.0', '2018-08-04 12:53:58'),
(25, 55, 'Laporan Jadwal Tidak Sesuai\nKelas: IK-3B\nMata Kuliah: Tugas Akhir\n\nSaran Perubahan\nJam Mulai: 07:00\nJam Selesai: 16:00', '2018-08-05 14:28:15'),
(26, 24, 'Laporan Jadwal Tidak Sesuai\nKelas: \nMata Kuliah: Pemrograman Aplikasi Mobile\n\nSaran Perubahan\nJam Mulai: 14:00\nJam Selesai: 18:00', '2018-08-06 00:30:38'),
(27, 49, 'Laporan Jadwal Tidak Sesuai\nKelas: IK-3B\nMata Kuliah: Sistem Informasi Enterprise (T)\n\nSaran Perubahan\nJam Mulai: 12:30\nJam Selesai: 14:00', '2018-08-06 04:11:37'),
(29, 49, 'Laporan Jadwal Tidak Sesuai\nKelas: IK-3B\nMata Kuliah: Bahasa Inggris IV\n\nSaran Perubahan\nJam Mulai: 07:00\nJam Selesai: 08:30', '2018-08-08 00:51:35'),
(30, 29, 'hhfhfhhf', '2018-08-09 03:32:55');

-- --------------------------------------------------------

--
-- Struktur dari tabel `firebase`
--

CREATE TABLE `firebase` (
  `id_firebase` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `topic` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `firebase`
--

INSERT INTO `firebase` (`id_firebase`, `id_user`, `token`, `topic`) VALUES
(1, 1, 'dK3K7CtrCzI:APA91bFndqL8lR6cspCbD5bH0o9VE5eW2Tahvw28dkaMc4I0wOzpbygvGdtlQA_HDYeW8GW44mxSsDJCeBX_cNiF0oUI3uH8mgixoYhkw1Ow4w1JXKTBBgOquSyA5H7KktlIVIm9qw21ZbGuZaH2FVBsqf7hgXUcvg', 'global,mahasiswa,IK-3A'),
(2, 4, 'ec2lG4yWhHM:APA91bFd39NoOl7foQaxltisQUjBGGzCuV1juBK9fSQAyXwkNezB5YZYCPtxJGZHm-WuUe-0x5U0_c5zLMjYuCIfOcZJ5BPn6k64hxItpY_HTmokps3VvZTuyLwFV0fIvjgpuxSvrOPOqjR5AchDf94lm3b0vYc_2Q', 'global,dosen,kaprodi'),
(3, 2, 'cjBm9nrRUC0:APA91bFBMbDn_cFixgp95wA602A0mIbuOiMAokphOc8ZUPwpQNNcrnRLwrV1vlfmLO8Ee9bKRjEf9RjTwYk4PnvXyqiQDdGNQ9UJJQYoasyQkxt0x1PNQhvBkRcddnHyvfz3Ve4xUmXjvbMIX3FWO_7fZU3N17tWDA', 'global,mahasiswa,IK-3B'),
(4, 26, 'f9fAlCnP3Pc:APA91bFoIhKeWRAct4A0Ccs8H3CvHUWJSIINqIs3qQ6T0ZvC7froZQVZGYY5JLMxOlS658RUb0QrzQkKOPuvWr7an6DWMO8jkBfBrZOCJJB6tqrn4vJMUccMHwYWD6hkWCtC84DJes-arKcbHQjFUrZkPxtRF677CQ', 'global,mahasiswa,IK-3A'),
(5, 43, 'eXlU59XDYos:APA91bFdxHtl85Qr0f772ApYxJ9BhJ2NHhteFkr_NYwjpIVLGuxcoI-Q8wkyzh8V20vW0LLJx5Dix8As3CfeaMx3', 'global,mahasiswa,IK-1A'),
(6, 24, 'e2D0sz1B4qM:APA91bF3a0SnxNG6_HK_vVHc-L2W75YboukuqO3dMTBWwNWdMqMplFP7ABffUnVhtnUNwWMOdSbe0GlXvfzay34sCIdu_KOqxPHNylvwnooH9i7jRcxvAYz5s7uNLl0v_6ie_mLNjVFo2G1wgL5q9R8QwPn1eN4kRw', 'global,dosen'),
(7, 5, 'd0v13c-KLBc:APA91bGSeDkMTG-xgLjINuVoRJMCMvMN6wyTF0QpdIg5HYtFzEtYzZYlRQGx4hkWpQKj0UloKFLel9IDYIOj-sIr0l4aGmrER2Yen3IcmjjWVZpwZ9j0QHDISvJMykfPhOT5Gps3TYTRx7GpZWfJb5oJgXNXuDbK4g', 'global,dosen'),
(8, 44, 'cX270k0r3uc:APA91bGkJbcAHFFuuFvI8MR_JKCnZYu2Tm4mRDqoI-rLbVAnMP5TDsne3jMCFndg4tLO1AT12kDPc2Z59XCF25NZBahtwT5EdAzoZ7GuwWswthF47PgkDwOAas2ye3ytZ4z7xmMgF3yyonj3TzwgdU57pDf48r819A', 'global,mahasiswa,IK-2A'),
(9, 32, 'foFRbh5WQM0:APA91bEHeKDdCTK-LkJENFJQDMfPJZlGQPuLFd89Uww_T1AcXJxSXE-PCf4glYbwhqA2i2a5XUpLb2dB6DxN9-yd', 'global,mahasiswa,IK-3A'),
(10, 17, 'drvuvFUYBZ0:APA91bFjMMB1z4o8JF05F4w_5UuqYIinDpdgUxyCyU_4ttayJOFFDMPM0CdG04-HO4zGIzHymCOrCqg6aXh08hvp8r-N_BMgnmEINB3LI9tGDNor6_hnT4VDjbX435QC9-TckxJCW-szbybOXozr-aUWcKQPJgi-KA', 'global,dosen'),
(11, 19, 'ddNl9c5gwpw:APA91bG0Dea4uptKxDmJz49NnZN2uV6S9MU4MNcVdmKJNF1ew8RzsOPE4v7JtUcPOnnPELcKk1MYuC3aPzOPrZXXytnetC7BGj7pGJzezNBZb42v7cIwCt1t_wjuvJ217v0iruS5MpdpWEXka_-GtMH1XG3_pAp6TA', 'global,dosen'),
(12, 21, 'fViQPUdwkqA:APA91bG2AHs0bDUahZHeXqnULjN__Uu0dLyTBtHm3FM2pIwl9CMj0jK73NGaYkCnAC38Dz92q9mUiOaLpmKRgSqdvQzzoN_3Cn4z83uQHWsgy07jiMl_fIcf98AJPggx_F73RfP63qUnYPMeXm6lx1pQw0YBoOlcgg', 'global,dosen'),
(13, 18, 'e6Ss0kP8ZhY:APA91bHstdZjWvI5SJGiMmxP4zkwndcgTQVj_TafhdLucRRgXvNdIVLwmzkAmoLi7uwjCE2ZU-A5mN7fhhq7POuR2hrYHODpcJUH-phIehh8R1CvOD5FAzYTaF9a9bh6S0nSvmNIni139mrckZghqytXAHb2CKayNw', 'global,dosen'),
(14, 40, 'dnW6zeyMx2M:APA91bEoA62fTPeVEmmhkt22KxOPwgqRrkuaBPw-X9pNwWwjdbd1xVZ0uP82_2NRBj686PeDZj4-lizk9eCvqMe7WZ8Bx4m_JQdAFeXLtAIqaSQmL2s9bFaEWbZrvPrwhoISv_4skxZgcxcZrGPXlUOrpJvTgM8EZg', 'global,mahasiswa,IK-3A'),
(15, 29, 'doOlBaa_HjA:APA91bH84kTrCHZFwLIvS4vPDXdagVxp0uJBCO8RirPKCPsN-ZGtvBOrsl8FdRIPzKCd7MGhGtePRLVQncOfRAh-cDzKkU0SrtRfmL4u-NPVoVakiVZj8qBHjc8bCPiSjeCRQGbXWaYG-y_3p2hNnvsUMLtkRRQ0OQ', 'global,mahasiswa,IK-3A'),
(16, 39, 'dauXtaKgZws:APA91bFjNl77DVF_X-qQazWMLgLAko8CgH85pTO6cjRe9QB7rMLewq_zfX1EqFqxZT44Blm0oHvMwRjyNHMkRtf-d1IAc_UexOqDfQLUMssqJZkf5BtpeK3qnNmmBP9sU6HYKVhOKcc9aXcp3vYEOgnVRpYp51fGdw', 'global,mahasiswa,IK-3A'),
(17, 33, 'dfHJPFKHtt0:APA91bFm9PYsyTPZdXzJ3fFbjmd_IHXiNJjsawfZA024yQtTLU8aGXeYJuekwni5_C0HrokvB2Y6wGD4eI2JWm42C0wKuIjJzZKIsQVVVEraqYzSbXJUpd3uuoCcS_hWgAhc3IXfTVd5OH3j_0HF2IXnDIdf9bq1WQ', 'global,mahasiswa,IK-3A'),
(18, 56, 'dDIZ4sp9eME:APA91bEjq5zURLfQ4xEh7V83ia3XpOfh_lGzxe_XoUVkNWkmwYJsKZJ68wusMOdYaZdOt4oy5fa1w4bgDjyMF1Luyl12QnnByYsepFfuy-D59lk02tPO8Vm7wi8rl8X3rJRgJ0uNadXZVvV8GW5uZZQVnt7e9F-UhA', 'global,mahasiswa,IK-3B'),
(19, 64, 'cEISniioioU:APA91bEE0AwIu_LnkeXf2jOgal60mS9vU0fKa_kJHlu3cXoifIwYBR27tdS9A3PfB8BAyHACjwvS5vDfyKPeauvyKm6mUZKIHs2PVrWL6lKdfM7CwaFFj_RyKu7K_vfU0A9bMo4nRwtVbtFiG3Kmj-cs6itcefmo2A', 'global,mahasiswa,IK-3B'),
(20, 49, 'cY1u2ai7vJM:APA91bGFTtTa0zOzPey6cbWs1-jPeY0XtcGP6dIj4UXeZ7l5XMtu5rC9EASIhzXBLVvXDL2cKw-PObog9cwaISnRNoHGXgS4hLqODSsr7caXwYSbXH5JNy5r0UU7vDx0ok4AaGbxY0k4RUudvIBc73F0rT9pSie2_A', 'global,mahasiswa,IK-3B'),
(21, 50, 'fVj9cVDDLa8:APA91bGMxflPBzUwjvcxYgADjlif4AVsl6ix6tE8Bpa37fhJD1cIhd7F2eATp-uhTrEd0EOsC2aR7bM6iYDt1TvgG7IN34lbgG4-C9ZmBvnwvD2CXifejQ_YYjgHKvxgmwyWg9VuYXS795_8FT2SxusfwuFat6T0vw', 'global,mahasiswa,IK-3B'),
(22, 53, 'fJ23Ok9pn4g:APA91bGE-2r5fJu8PXIDtH8Z2dyNarVVSYRRM4esxaZVD5z-nNv7xHp_GnHlHCvkxA8bnMGY_3X1A1_Z1Vzer4_IvI4M7dKZJmAs_zYJ-O8sPEe-WpydhYjlHNBYrurKvAmyUcps1Hpb2BrrHbgtw6kwOLx5qYNrug', 'global,mahasiswa,IK-3B'),
(23, 55, 'elKNRWPR6W8:APA91bGqDCnL13sof7ObBjQfaa4zdzbB2y1l6s_q81SM8-503k2BF-ZjnCVCv_YbbKD1ANb0t2AVVxkNO7YFJWsQM7osI-O7hCfLB8bMKeQoHuep6cf8IeTyzrGQ2fAnesos89XekQ-I5pXRPvSAC9swAK4WGNCAhg', 'global,mahasiswa,IK-3B'),
(24, 46, 'eszpnm2qzZ0:APA91bEMRSW0iOo1iuIvlAtVOhta4IWvW47g6m4wElao-7onQ6Ae51pau0dRzJCYQMkgSqhJjPDKVbWtdnf4_2fFS6CPLq_g5N1vBqm9l52f-WF-c5avPmyBKO9FWiXStXw_0_TZ3fy-JyHDcDy3ICqnA37JAPzFBA', 'global,mahasiswa,IK-3B'),
(25, 60, 'fH5Dvt67dZE:APA91bGPdvOazeO2_ARbWCD60Xq_FRp1L8ff-UpM-IiKETHNNK8aFx7GKv9dFS8swY2WBhNn55UVapI0XQulMQWvyghltx9TMH5gy_wlDcvqa72lv0JM885HwUbqGngIrJVu4tNYwXT0U38PQu24tck15Tg9guH3lQ', 'global,mahasiswa,IK-3B');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hari`
--

CREATE TABLE `hari` (
  `id_hari` int(11) NOT NULL,
  `hari` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `hari`
--

INSERT INTO `hari` (`id_hari`, `hari`) VALUES
(1, 'Senin'),
(2, 'Selasa'),
(3, 'Rabu'),
(4, 'Kamis'),
(5, 'Jumat');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jadwal`
--

CREATE TABLE `jadwal` (
  `id_jadwal` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL,
  `matakuliah` text NOT NULL,
  `pengampu` int(11) NOT NULL,
  `ruang` int(11) NOT NULL,
  `hari` int(11) NOT NULL,
  `jam_mulai` time NOT NULL,
  `jam_selesai` time NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jadwal`
--

INSERT INTO `jadwal` (`id_jadwal`, `id_kelas`, `id_semester`, `matakuliah`, `pengampu`, `ruang`, `hari`, `jam_mulai`, `jam_selesai`, `status`) VALUES
(1, 1, 2, 'Desain Grafis Lanjut (TP)', 6, 3, 1, '10:20:00', '03:30:00', 1),
(2, 1, 2, 'Komunikasi Data (TP)', 15, 5, 1, '17:30:00', '19:30:00', 1),
(3, 1, 2, 'Pendidikan Agama', 7, 5, 2, '10:20:00', '11:50:00', 1),
(4, 1, 2, 'Pemrograman Visual (TP)', 8, 4, 2, '14:00:00', '17:30:00', 1),
(5, 1, 2, 'Sistem Basis Data Dasar (T)', 8, 5, 3, '12:30:00', '14:00:00', 1),
(6, 1, 2, 'Statistika (T)', 14, 5, 3, '14:00:00', '15:30:00', 1),
(7, 1, 2, 'Komunikasi Data (TP)', 15, 5, 3, '16:30:00', '19:30:00', 1),
(8, 1, 2, 'PBO (TP)', 4, 3, 4, '10:20:00', '15:30:00', 1),
(9, 1, 2, 'Pancasila', 16, 5, 4, '16:00:00', '17:30:00', 1),
(10, 1, 2, 'Sistem Basis Data Dasar (P)', 8, 2, 5, '07:00:00', '10:20:00', 1),
(11, 1, 2, 'Metode Numerik', 23, 5, 5, '10:20:00', '11:50:00', 1),
(12, 2, 2, 'Sistem Basis Data Dasar (TP)', 8, 4, 1, '07:00:00', '11:50:00', 1),
(13, 2, 2, 'Pancasila', 16, 5, 1, '14:00:00', '15:30:00', 1),
(14, 2, 2, 'Pendidikan Agama', 7, 5, 2, '08:30:00', '10:20:00', 1),
(15, 2, 2, 'Pemrograman Visual (TP)', 8, 4, 2, '10:20:00', '14:00:00', 1),
(16, 2, 2, 'Komunikasi Data (TP)', 15, 5, 2, '17:30:00', '19:30:00', 1),
(17, 2, 2, 'PBO (T)', 4, 5, 3, '07:00:00', '08:30:00', 1),
(18, 2, 2, 'Komunikasi Data (TP)', 15, 2, 3, '08:30:00', '14:00:00', 1),
(19, 2, 2, 'PBO (P)', 4, 3, 4, '07:00:00', '10:20:00', 1),
(20, 2, 2, 'Statistika', 14, 5, 4, '10:20:00', '11:50:00', 1),
(21, 2, 2, 'Desain Grafis Lanjut (Tp)', 6, 5, 5, '07:00:00', '11:50:00', 1),
(22, 2, 2, 'Metode Numerik', 23, 5, 5, '13:30:00', '15:00:00', 1),
(23, 3, 4, 'Bahasa Indonesia', 17, 5, 1, '08:30:00', '10:20:00', 1),
(24, 3, 4, 'Bahasa Inggris II', 21, 6, 1, '12:30:00', '14:00:00', 1),
(25, 3, 4, 'Perancangan Sistem Informasi (TP)', 4, 3, 1, '14:00:00', '14:45:00', 1),
(26, 3, 4, 'Animasi dan Desain Grafis (T)', 18, 5, 2, '12:30:00', '14:00:00', 1),
(27, 3, 4, 'Mikroprosesor & Antarmuka (T)', 19, 2, 2, '14:00:00', '15:30:00', 1),
(28, 3, 4, 'Mikroprosesor & Antarmuka (P)', 19, 2, 2, '16:45:00', '20:15:00', 1),
(29, 3, 4, 'Pemrograman Web Berbais Framework (TP)', 18, 4, 3, '14:00:00', '19:30:00', 1),
(30, 3, 4, 'Pemrograman Basis Data', 20, 4, 4, '12:30:00', '17:30:00', 1),
(31, 3, 4, 'Animasi dan Desain Grafis (P)', 18, 3, 4, '17:30:00', '20:15:00', 1),
(32, 3, 4, 'Jaringan Komputer II (TP)', 14, 2, 5, '13:30:00', '18:15:00', 1),
(33, 4, 4, 'Bahasa Indonesia', 17, 5, 1, '17:00:00', '18:30:00', 1),
(34, 4, 4, 'Bahasa Inggris II', 21, 6, 1, '08:30:00', '10:20:00', 1),
(35, 4, 4, 'Perancangan Sistem Informasi (T)', 4, 5, 1, '10:20:00', '11:50:00', 1),
(36, 4, 4, 'Mikroprosesor & Antarmuka (T)', 19, 5, 1, '14:45:00', '16:45:00', 1),
(37, 4, 4, 'Perancangan Sistem Informasi (P)', 4, 3, 2, '07:00:00', '09:15:00', 1),
(38, 4, 4, 'Pemrograman Basis Data', 20, 3, 2, '09:35:00', '15:30:00', 1),
(39, 4, 4, 'Animasi dan Desain Grafis (TP)', 18, 3, 3, '07:00:00', '11:05:00', 1),
(40, 4, 4, 'Mikroprosesor & Antarmuka (P)', 19, 3, 3, '11:05:00', '15:30:00', 1),
(41, 4, 4, 'Jaringan Komputer II (TP)', 14, 2, 4, '07:00:00', '11:50:00', 1),
(42, 4, 4, 'Pemrogaman Web Berbasis Framework (TP)', 5, 4, 5, '07:00:00', '11:50:00', 1),
(43, 5, 6, 'Sistem Manajemen Mutu', 15, 5, 1, '12:30:00', '14:00:00', 1),
(44, 5, 6, 'Pemrograman Aplikasi Mobile', 5, 4, 1, '16:00:00', '21:00:00', 1),
(45, 5, 6, 'Sistem Manajemen Mutu', 15, 5, 2, '07:00:00', '08:30:00', 1),
(46, 5, 6, 'Bahasa Inggris IV', 21, 6, 2, '08:30:00', '10:20:00', 1),
(47, 5, 6, 'Man. Internetworking & Router', 20, 2, 2, '12:30:00', '15:30:00', 1),
(48, 5, 6, 'Man. Internetworking & Router', 6, 2, 3, '14:00:00', '18:15:00', 1),
(49, 5, 6, 'Sistem Informasi Enterprise', 22, 4, 4, '07:00:00', '11:50:00', 1),
(50, 5, 6, 'K3, Hukum, & Etika Profesi', 15, 5, 4, '12:30:00', '14:00:00', 1),
(51, 5, 6, 'Tugas Akhir', 24, 1, 5, '07:00:00', '15:00:00', 1),
(52, 6, 6, 'Man. Internetworking & Router', 20, 2, 1, '07:00:00', '10:20:00', 1),
(53, 6, 6, 'Sistem Manajemen Mutu', 15, 6, 1, '10:20:00', '11:50:00', 1),
(54, 6, 6, 'Sistem Informasi Enterprise (T)', 23, 5, 1, '12:30:00', '14:00:00', 1),
(55, 6, 6, 'Bahasa Inggris IV', 21, 6, 2, '07:00:00', '08:30:00', 1),
(56, 6, 6, 'Sistem Informasi Enterprise (P)', 22, 2, 2, '08:30:00', '11:50:00', 1),
(57, 6, 6, 'Pemrograman Aplikasi Mobile', 5, 4, 3, '07:00:00', '13:15:00', 1),
(58, 6, 6, 'K3, Hukum, & Etika Profesi', 15, 5, 4, '09:35:00', '11:05:00', 1),
(59, 6, 6, 'Man. Internetworking & Router', 6, 2, 4, '12:30:00', '15:30:00', 1),
(60, 6, 6, 'Tugas Akhir', 24, 1, 5, '07:00:00', '16:00:00', 1),
(70, 1, 1, '111', 4, 1, 1, '05:30:00', '05:30:00', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelas`
--

CREATE TABLE `kelas` (
  `id_kelas` int(11) NOT NULL,
  `kelas` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `kelas`) VALUES
(1, 'IK-1A'),
(2, 'IK-1B'),
(3, 'IK-2A'),
(4, 'IK-2B'),
(5, 'IK-3A'),
(6, 'IK-3B'),
(99, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengumuman`
--

CREATE TABLE `pengumuman` (
  `id_pengumuman` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `pengumuman` text NOT NULL,
  `target` int(11) NOT NULL,
  `pengirim` int(11) NOT NULL,
  `upload_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengumuman`
--

INSERT INTO `pengumuman` (`id_pengumuman`, `judul`, `pengumuman`, `target`, `pengirim`, `upload_date`) VALUES
(1, 'Pengumpulan Tugas Besar MIR', 'Untuk Mahasiswa IK-3A\r\nLorem ipsum dolor sit amet, ut duo melius doctus consequuntur, vis id aeterno officiis disputando, duo atqui salutandi id. Sed ei legere vidisse, ius no amet modo ignota, congue offendit vim ne. His ut dicat laoreet voluptatum. Integre dolores contentiones nec ei, tation definitiones ea mel.', 5, 6, '2018-05-27 07:56:29'),
(2, 'Pengumpulan Tugas Besar MIR', 'Untuk Mahasiswa IK-3B\r\nLorem ipsum dolor sit amet, ut duo melius doctus consequuntur, vis id aeterno officiis disputando, duo atqui salutandi id. Sed ei legere vidisse, ius no amet modo ignota, congue offendit vim ne. His ut dicat laoreet voluptatum. Integre dolores contentiones nec ei, tation definitiones ea mel.', 6, 6, '2018-05-27 07:57:44'),
(3, 'Coba Pengumuman Untuk Semua', 'Untuk Semua\r\nLorem ipsum dolor sit amet, ut duo melius doctus consequuntur, vis id aeterno officiis disputando, duo atqui salutandi id. Sed ei legere vidisse, ius no amet modo ignota, congue offendit vim ne. His ut dicat laoreet voluptatum. Integre dolores contentiones nec ei, tation definitiones ea mel.', 99, 4, '2018-05-27 07:58:22'),
(4, 'Coba Pengumuman Untuk Semua', 'Untuk Semua\r\nLorem ipsum dolor sit amet, ut duo melius doctus consequuntur, vis id aeterno officiis disputando, duo atqui salutandi id. Sed ei legere vidisse, ius no amet modo ignota, congue offendit vim ne. His ut dicat laoreet voluptatum. Integre dolores contentiones nec ei, tation definitiones ea mel.', 99, 5, '2018-05-27 07:58:22'),
(5, 'Coba Pengumuman Untuk Semua', 'Untuk Semua\r\nLorem ipsum dolor sit amet, ut duo melius doctus consequuntur, vis id aeterno officiis disputando, duo atqui salutandi id. Sed ei legere vidisse, ius no amet modo ignota, congue offendit vim ne. His ut dicat laoreet voluptatum. Integre dolores contentiones nec ei, tation definitiones ea mel.', 99, 15, '2018-05-27 07:58:22'),
(6, 'Coba Pengumuman Untuk Semua', 'Untuk Semua\r\nLorem ipsum dolor sit amet, ut duo melius doctus consequuntur, vis id aeterno officiis disputando, duo atqui salutandi id. Sed ei legere vidisse, ius no amet modo ignota, congue offendit vim ne. His ut dicat laoreet voluptatum. Integre dolores contentiones nec ei, tation definitiones ea mel.', 99, 18, '2018-05-27 07:58:22'),
(7, 'Pengumuman Daftar Pembimbing TA', 'Untuk Mahasiswa IK-3A\r\nLorem ipsum dolor sit amet, ut duo melius doctus consequuntur, vis id aeterno officiis disputando, duo atqui salutandi id. Sed ei legere vidisse, ius no amet modo ignota, congue offendit vim ne. His ut dicat laoreet voluptatum. Integre dolores contentiones nec ei, tation definitiones ea mel.', 5, 4, '2018-05-27 07:56:29'),
(8, 'Pengumuman Daftar Pembimbing TA', 'Untuk Mahasiswa IK-3B\r\nLorem ipsum dolor sit amet, ut duo melius doctus consequuntur, vis id aeterno officiis disputando, duo atqui salutandi id. Sed ei legere vidisse, ius no amet modo ignota, congue offendit vim ne. His ut dicat laoreet voluptatum. Integre dolores contentiones nec ei, tation definitiones ea mel.', 6, 4, '2018-05-27 07:57:44'),
(10, 'Coba Pengumuman Untuk Semua', 'Untuk Semua\r\nLorem ipsum dolor sit amet, ut duo melius doctus consequuntur, vis id aeterno officiis disputando, duo atqui salutandi id. Sed ei legere vidisse, ius no amet modo ignota, congue offendit vim ne. His ut dicat laoreet voluptatum. Integre dolores contentiones nec ei, tation definitiones ea mel.', 99, 4, '2018-05-27 07:58:22'),
(11, 'Coba Pengumuman Untuk Semua', 'Untuk Semua\r\nLorem ipsum dolor sit amet, ut duo melius doctus consequuntur, vis id aeterno officiis disputando, duo atqui salutandi id. Sed ei legere vidisse, ius no amet modo ignota, congue offendit vim ne. His ut dicat laoreet voluptatum. Integre dolores contentiones nec ei, tation definitiones ea mel.', 99, 5, '2018-05-27 07:58:22'),
(12, 'Coba Pengumuman Untuk Semua', 'Untuk Semua\r\nLorem ipsum dolor sit amet, ut duo melius doctus consequuntur, vis id aeterno officiis disputando, duo atqui salutandi id. Sed ei legere vidisse, ius no amet modo ignota, congue offendit vim ne. His ut dicat laoreet voluptatum. Integre dolores contentiones nec ei, tation definitiones ea mel.', 99, 15, '2018-05-27 07:58:22'),
(13, 'Coba Pengumuman Untuk Semua', 'Untuk Semua\r\nLorem ipsum dolor sit amet, ut duo melius doctus consequuntur, vis id aeterno officiis disputando, duo atqui salutandi id. Sed ei legere vidisse, ius no amet modo ignota, congue offendit vim ne. His ut dicat laoreet voluptatum. Integre dolores contentiones nec ei, tation definitiones ea mel.', 99, 18, '2018-05-27 07:58:22'),
(24, 'Uji Ulang Pemrograman Mobile', 'Untuk Mahasiswa IK-3A\r\nLorem ipsum dolor sit amet, ut duo melius doctus consequuntur, vis id aeterno officiis disputando, duo atqui salutandi id. Sed ei legere vidisse, ius no amet modo ignota, congue offendit vim ne. His ut dicat laoreet voluptatum. Integre dolores contentiones nec ei, tation definitiones ea mel.', 5, 5, '2018-05-27 07:56:29'),
(25, 'Uji Ulang Pemrograman Mobile', 'Untuk Mahasiswa IK-3B\r\nLorem ipsum dolor sit amet, ut duo melius doctus consequuntur, vis id aeterno officiis disputando, duo atqui salutandi id. Sed ei legere vidisse, ius no amet modo ignota, congue offendit vim ne. His ut dicat laoreet voluptatum. Integre dolores contentiones nec ei, tation definitiones ea mel.', 6, 5, '2018-05-27 07:57:44'),
(26, 'Pengujian Notifikasi', 'Uji Notifikasi Pengumuman\n\ntdhjvjv\nhgjbjjkfcv\n228688\n38666\n-*/()/_@(__(#&%', 5, 24, '2018-08-04 20:33:03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `posisi`
--

CREATE TABLE `posisi` (
  `id_posisi` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `last_sync` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `posisi`
--

INSERT INTO `posisi` (`id_posisi`, `id_user`, `id_ruang`, `last_sync`) VALUES
(1, 4, 1, '2018-05-27 01:01:46'),
(2, 5, 1, '2018-05-27 01:01:59'),
(4, 24, 1, '2018-08-10 03:54:48');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ruang`
--

CREATE TABLE `ruang` (
  `id_ruang` int(11) NOT NULL,
  `ruang` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ruang`
--

INSERT INTO `ruang` (`id_ruang`, `ruang`) VALUES
(1, 'Ruang Dosen'),
(2, 'Lab. Jaringan Komputer'),
(3, 'Lab. Multimedia'),
(4, 'Lab. Pemrograman'),
(5, 'SB I 04'),
(6, 'MST'),
(99, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `semester`
--

CREATE TABLE `semester` (
  `id_semester` int(11) NOT NULL,
  `semester` int(11) NOT NULL,
  `ganjil_genap` enum('ganjil','genap') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `semester`
--

INSERT INTO `semester` (`id_semester`, `semester`, `ganjil_genap`) VALUES
(1, 1, 'ganjil'),
(2, 2, 'genap'),
(3, 3, 'ganjil'),
(4, 4, 'genap'),
(5, 5, 'ganjil'),
(6, 6, 'genap');

-- --------------------------------------------------------

--
-- Struktur dari tabel `surat`
--

CREATE TABLE `surat` (
  `id_surat` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `jenis` varchar(20) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `foto` text NOT NULL,
  `tanggal_surat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `surat`
--

INSERT INTO `surat` (`id_surat`, `id_user`, `jenis`, `judul`, `foto`, `tanggal_surat`, `tanggal`) VALUES
(1, 4, 'Surat Pemberitahuan', 'Permohonan Pengisian Kuisioner', 'Surat_Pemberitahuan_1.jpg;Surat_Pemberitahuan_2.jpg;', '2018-08-02 02:00:00', '2018-08-02 01:14:00'),
(2, 5, 'Surat Pemberitahuan', 'Permohonan Pengisian Kuisioner', 'Surat_Pemberitahuan_1.jpg;Surat_Pemberitahuan_2.jpg;', '2018-08-02 02:00:00', '2018-08-02 01:14:00'),
(3, 6, 'Surat Pemberitahuan', 'Permohonan Pengisian Kuisioner', 'Surat_Pemberitahuan_1.jpg;Surat_Pemberitahuan_2.jpg;', '2018-08-02 02:00:00', '2018-08-02 01:14:00'),
(4, 7, 'Surat Pemberitahuan', 'Permohonan Pengisian Kuisioner', 'Surat_Pemberitahuan_1.jpg;Surat_Pemberitahuan_2.jpg;', '2018-08-02 02:00:00', '2018-08-02 01:14:00'),
(5, 8, 'Surat Pemberitahuan', 'Permohonan Pengisian Kuisioner', 'Surat_Pemberitahuan_1.jpg;Surat_Pemberitahuan_2.jpg;', '2018-08-02 02:00:00', '2018-08-02 01:14:00'),
(6, 14, 'Surat Pemberitahuan', 'Permohonan Pengisian Kuisioner', 'Surat_Pemberitahuan_1.jpg;Surat_Pemberitahuan_2.jpg;', '2018-08-02 02:00:00', '2018-08-02 01:14:00'),
(7, 15, 'Surat Pemberitahuan', 'Permohonan Pengisian Kuisioner', 'Surat_Pemberitahuan_1.jpg;Surat_Pemberitahuan_2.jpg;', '2018-08-02 02:00:00', '2018-08-02 01:14:00'),
(8, 16, 'Surat Pemberitahuan', 'Permohonan Pengisian Kuisioner', 'Surat_Pemberitahuan_1.jpg;Surat_Pemberitahuan_2.jpg;', '2018-08-02 02:00:00', '2018-08-02 01:14:00'),
(9, 17, 'Surat Pemberitahuan', 'Permohonan Pengisian Kuisioner', 'Surat_Pemberitahuan_1.jpg;Surat_Pemberitahuan_2.jpg;', '2018-08-02 02:00:00', '2018-08-02 01:14:00'),
(10, 18, 'Surat Pemberitahuan', 'Permohonan Pengisian Kuisioner', 'Surat_Pemberitahuan_1.jpg;Surat_Pemberitahuan_2.jpg;', '2018-08-02 02:00:00', '2018-08-02 01:14:00'),
(11, 19, 'Surat Pemberitahuan', 'Permohonan Pengisian Kuisioner', 'Surat_Pemberitahuan_1.jpg;Surat_Pemberitahuan_2.jpg;', '2018-08-02 02:00:00', '2018-08-02 01:14:00'),
(12, 20, 'Surat Pemberitahuan', 'Permohonan Pengisian Kuisioner', 'Surat_Pemberitahuan_1.jpg;Surat_Pemberitahuan_2.jpg;', '2018-08-02 02:00:00', '2018-08-02 01:14:00'),
(13, 21, 'Surat Pemberitahuan', 'Permohonan Pengisian Kuisioner', 'Surat_Pemberitahuan_1.jpg;Surat_Pemberitahuan_2.jpg;', '2018-08-02 02:00:00', '2018-08-02 01:14:00'),
(14, 22, 'Surat Pemberitahuan', 'Permohonan Pengisian Kuisioner', 'Surat_Pemberitahuan_1.jpg;Surat_Pemberitahuan_2.jpg;', '2018-08-02 02:00:00', '2018-08-02 01:14:00'),
(15, 23, 'Surat Pemberitahuan', 'Permohonan Pengisian Kuisioner', 'Surat_Pemberitahuan_1.jpg;Surat_Pemberitahuan_2.jpg;', '2018-08-02 02:00:00', '2018-08-02 01:14:00'),
(16, 24, 'Surat Pemberitahuan', 'Permohonan Pengisian Kuisioner', 'Surat_Pemberitahuan_1.jpg;Surat_Pemberitahuan_2.jpg;', '2018-08-02 02:00:00', '2018-08-02 01:14:00'),
(17, 4, 'Surat Undangan', 'Undangan Rapat Tahunan', 'Surat_Undangan_1.jpg;', '2018-08-02 23:15:00', '2018-08-02 23:29:37'),
(18, 5, 'Surat Undangan', 'Undangan Rapat Tahunan', 'Surat_Undangan_1.jpg;', '2018-08-02 23:15:00', '2018-08-02 23:29:37'),
(19, 6, 'Surat Undangan', 'Undangan Rapat Tahunan', 'Surat_Undangan_1.jpg;', '2018-08-02 23:15:00', '2018-08-02 23:29:37'),
(20, 7, 'Surat Undangan', 'Undangan Rapat Tahunan', 'Surat_Undangan_1.jpg;', '2018-08-02 23:15:00', '2018-08-02 23:29:37'),
(21, 8, 'Surat Undangan', 'Undangan Rapat Tahunan', 'Surat_Undangan_1.jpg;', '2018-08-02 23:15:00', '2018-08-02 23:29:37'),
(22, 14, 'Surat Undangan', 'Undangan Rapat Tahunan', 'Surat_Undangan_1.jpg;', '2018-08-02 23:15:00', '2018-08-02 23:29:37'),
(23, 15, 'Surat Undangan', 'Undangan Rapat Tahunan', 'Surat_Undangan_1.jpg;', '2018-08-02 23:15:00', '2018-08-02 23:29:37'),
(24, 16, 'Surat Undangan', 'Undangan Rapat Tahunan', 'Surat_Undangan_1.jpg;', '2018-08-02 23:15:00', '2018-08-02 23:29:37'),
(25, 17, 'Surat Undangan', 'Undangan Rapat Tahunan', 'Surat_Undangan_1.jpg;', '2018-08-02 23:15:00', '2018-08-02 23:29:37'),
(26, 18, 'Surat Undangan', 'Undangan Rapat Tahunan', 'Surat_Undangan_1.jpg;', '2018-08-02 23:15:00', '2018-08-02 23:29:37'),
(27, 19, 'Surat Undangan', 'Undangan Rapat Tahunan', 'Surat_Undangan_1.jpg;', '2018-08-02 23:15:00', '2018-08-02 23:29:37'),
(28, 20, 'Surat Undangan', 'Undangan Rapat Tahunan', 'Surat_Undangan_1.jpg;', '2018-08-02 23:15:00', '2018-08-02 23:29:37'),
(29, 21, 'Surat Undangan', 'Undangan Rapat Tahunan', 'Surat_Undangan_1.jpg;', '2018-08-02 23:15:00', '2018-08-02 23:29:37'),
(30, 22, 'Surat Undangan', 'Undangan Rapat Tahunan', 'Surat_Undangan_1.jpg;', '2018-08-02 23:15:00', '2018-08-02 23:29:37'),
(31, 23, 'Surat Undangan', 'Undangan Rapat Tahunan', 'Surat_Undangan_1.jpg;', '2018-08-02 23:15:00', '2018-08-02 23:29:37'),
(32, 24, 'Surat Undangan', 'Undangan Rapat Tahunan', 'Surat_Undangan_1.jpg;', '2018-08-02 23:15:00', '2018-08-02 23:29:37');

-- --------------------------------------------------------

--
-- Struktur dari tabel `surat_tidak_mengajar`
--

CREATE TABLE `surat_tidak_mengajar` (
  `id_surat` int(11) NOT NULL,
  `pengusul` int(11) NOT NULL,
  `pengganti` int(11) NOT NULL,
  `jadwal` int(11) NOT NULL,
  `alasan` text NOT NULL,
  `keterangan` text NOT NULL,
  `status` int(11) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `surat_tidak_mengajar`
--

INSERT INTO `surat_tidak_mengajar` (`id_surat`, `pengusul`, `pengganti`, `jadwal`, `alasan`, `keterangan`, `status`, `tanggal`) VALUES
(1, 24, 4, 60, 'Keperluan keluarga', 'Beri tugas cari makalah tentang diagram perancangan', 1, '2018-08-06 04:39:51'),
(2, 24, 4, 60, 'vggg', 'ffff', 1, '2018-08-06 07:53:27');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sync`
--

CREATE TABLE `sync` (
  `id_sync` int(11) NOT NULL,
  `nama_tabel` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `sync`
--

INSERT INTO `sync` (`id_sync`, `nama_tabel`, `last_modified`) VALUES
(1, 'jadwal', '2018-08-12 10:38:35'),
(2, 'berita', '2018-05-01 12:41:58');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `nomor_induk` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `tipe` enum('Dosen','Mahasiswa','Ketua Program Studi') NOT NULL,
  `nama` varchar(255) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `last_sync` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `id_kelas`, `nomor_induk`, `password`, `tipe`, `nama`, `foto`, `last_sync`) VALUES
(1, 5, '33415023', '$2y$10$Pf1on9z5dmM8eHwfaCrxq.6Oqr/OmQXFdWMoldVBmxgyk50xUQzim', 'Mahasiswa', 'Thio Van Agusti', '33415023.jpg', '2018-08-11 04:41:49'),
(2, 6, '33415120', '$2y$10$.qd2ei6Az.XM7fkVzdWmFeyfn8/RKf1DKCOfZCSQ3TLAVc2b/diwK', 'Mahasiswa', 'Septyan Ajid Nugroho', '33415120.jpg', '2018-08-09 03:51:01'),
(4, 99, '197101172003121001', '$2y$10$VnHZK.uTI5I6agaHW7s7leAiqsHnNDs/V.KIHTFa/XjuJxSwsJX0u', 'Ketua Program Studi', 'SUKAMTO, S.Kom, M.T.', '197101172003121001.jpg', '2018-08-10 04:00:05'),
(5, 99, '198504102014041002', '$2y$10$dckZzc0CadyUViC4DaH.UO33u4w8rqm01bl4UxsNQCNwZYx3SBsFi', 'Dosen', 'PRAYITNO, SST., M.T.', '198504102014041002.jpg', '2018-08-09 04:03:39'),
(6, 99, '198404202015041003', '$2y$10$L.88ZsvRvvsWSuuS678Hc.VVY5sXfGu9Tp3ssNdvJblRwa3XDIR1m', 'Dosen', 'LILIEK TRIYONO, S.T., M.Kom.', 'default.png', '2018-08-04 09:23:31'),
(7, 99, '197206102000031001', '$2y$10$L.88ZsvRvvsWSuuS678Hc.VVY5sXfGu9Tp3ssNdvJblRwa3XDIR1m', 'Dosen', 'KHAMAMI, S.Ag., M.M.', 'default.png', '2018-08-04 09:23:31'),
(8, 99, '197501302001121001', '$2y$10$L.88ZsvRvvsWSuuS678Hc.VVY5sXfGu9Tp3ssNdvJblRwa3XDIR1m', 'Dosen', 'SLAMET HANDOKO, S. Kom, M. Kom.', 'default.png', '2018-08-04 09:23:31'),
(14, 99, '196008221988031001', '$2y$10$L.88ZsvRvvsWSuuS678Hc.VVY5sXfGu9Tp3ssNdvJblRwa3XDIR1m', 'Dosen', 'PARSUMO RAHARDJO, Drs, M. Kom.', 'default.png', '2018-08-04 09:23:31'),
(15, 99, '196810252000121001', '$2y$10$L.88ZsvRvvsWSuuS678Hc.VVY5sXfGu9Tp3ssNdvJblRwa3XDIR1m', 'Dosen', 'TRI RAHARJO YUDANTORO, S.Kom., M.Kom.', 'default.png', '2018-08-04 09:23:31'),
(16, 99, '197307082005011001', '$2y$10$L.88ZsvRvvsWSuuS678Hc.VVY5sXfGu9Tp3ssNdvJblRwa3XDIR1m', 'Dosen', 'TAUFIQ YULIANTO, S.H, M.H.', 'default.png', '2018-08-04 09:23:31'),
(17, 99, '196107101988112001', '$2y$10$L.88ZsvRvvsWSuuS678Hc.VVY5sXfGu9Tp3ssNdvJblRwa3XDIR1m', 'Dosen', 'NETTY NURDIYANI, Dra, M. Hum.', 'default.png', '2018-08-04 09:23:31'),
(18, 99, '197610032003121002', '$2y$10$L.88ZsvRvvsWSuuS678Hc.VVY5sXfGu9Tp3ssNdvJblRwa3XDIR1m', 'Dosen', 'BUDI SUYANTO, S.T, M. Eng.', 'default.png', '2018-08-04 09:23:31'),
(19, 99, '197704012005011001', '$2y$10$L.88ZsvRvvsWSuuS678Hc.VVY5sXfGu9Tp3ssNdvJblRwa3XDIR1m', 'Dosen', 'WAHYU SULISTIYO, S.T., M.Kom.', 'default.png', '2018-08-04 09:23:31'),
(20, 99, '197403112000121001', '$2y$10$L.88ZsvRvvsWSuuS678Hc.VVY5sXfGu9Tp3ssNdvJblRwa3XDIR1m', 'Dosen', 'MARDIYONO, S.Kom, M. Sc.', 'default.png', '2018-08-04 09:23:31'),
(21, 99, '195901191988031001', '$2y$10$L.88ZsvRvvsWSuuS678Hc.VVY5sXfGu9Tp3ssNdvJblRwa3XDIR1m', 'Dosen', 'SASONGKO, Drs, M. Hum.', 'default.png', '2018-08-04 09:23:31'),
(22, 99, '197711192008012013', '$2y$10$MU9/ZIOBkYvKI0CJmsucDe8D6r51hkjBBWKgk1frC.vHi.pVdSfZC', 'Dosen', 'IDHAWATI HESTININGSIH, S.Kom, M.Kom.', 'default.png', '2018-08-06 23:10:55'),
(23, 99, '197912272003122001', '$2y$10$L.88ZsvRvvsWSuuS678Hc.VVY5sXfGu9Tp3ssNdvJblRwa3XDIR1m', 'Dosen', 'ISWANTI, S.Si, M.Sc.', 'default.png', '2018-08-04 09:23:31'),
(24, 99, '99', '$2y$10$VsN1u7.llGBXNUH43Fijh.6JlkTgpwZt3btMEjLmi/hojmUe7yLkG', 'Dosen', 'Tim Dosen', 'default.png', '2018-08-04 13:28:02'),
(26, 5, '33415001', '$2y$10$6PeE82ApCdGaA3XLkeOnwOfCF.mG4emqJYf4hBH9Oi7Mjk4DJyrme', 'Mahasiswa', 'Annisa Kusumaningtyas', 'default.png', '2018-08-04 09:24:45'),
(27, 5, '33415002', '$2y$10$m/RXVBvlLXW8X8g8zIslv.wM1eprZigDeU0QKYExXJBxEGkacSqSi', 'Mahasiswa', 'Arini Islahul Ni''mah', 'default.png', '2018-08-04 09:24:45'),
(28, 5, '33415004', '$2y$10$PSRM8BWw.HAs92pOuxGTFuUKwnEnKUm7vhfYbHU9g0KYV3rUCtVgC', 'Mahasiswa', 'Darma Putra Pratama', 'default.png', '2018-08-04 09:24:45'),
(29, 5, '33415005', '$2y$10$g/6og5/Js569SF19eC2pce.QMRH3s70tKiJ/IqIYMGnQD4IfH6YO6', 'Mahasiswa', 'Dian Ramadhan', '33415005.jpg', '2018-08-09 01:12:09'),
(30, 5, '33415006', '$2y$10$CDU0SZriFYQdooJdLarkHuKz/5mWO5d30/3sZ.LiFJ9bDxVedeVri', 'Mahasiswa', 'Fitri Handayani', 'default.png', '2018-08-04 09:24:45'),
(31, 5, '33415007', '$2y$10$3VLem0gK4vg3LuA6Nkc5X.oQv4KrHIl8YTT9nj0w54ls4gkh6y4Ni', 'Mahasiswa', 'Fanny Fadhila Tusholiha', 'default.png', '2018-08-04 09:24:45'),
(32, 5, '33415010', '$2y$10$rxt4wNMSfTbOtT.JJQr6hOqVobA5KXG6IAVk0xMMu4z1yIcQKfr1q', 'Mahasiswa', 'Halimah Itsna Wardany', '33415010.jpg', '2018-08-04 09:24:45'),
(33, 5, '33415011', '$2y$10$4q4CPNWYU8N50LjF2ocv2.FZ7WWcnsYrOQaEhfwup.guNMtzhbVcm', 'Mahasiswa', 'Hesti Muji Rahayu', '33415011.png', '2018-08-04 12:29:25'),
(34, 5, '33415012', '$2y$10$7HvT8/3Sa3WDNwoMyQs7TuXssOB3x.jloIRKE5F1Mg/1epAqcqUv6', 'Mahasiswa', 'Kuwat Agus Setyowati', 'default.png', '2018-08-04 09:24:46'),
(35, 5, '33415013', '$2y$10$M4u4PeSzHh6v.PorDZgLduJ5kLvEpjtffM.ia0MYcMQljAV5oQTUq', 'Mahasiswa', 'Lutfi Sholikhati Kholishoh', 'default.png', '2018-08-04 09:24:46'),
(36, 5, '33415015', '$2y$10$sC89AD4e4woMK9HFt2M0cudg/2ZbvP5ylFDwxjMtJh1QR07zouVkW', 'Mahasiswa', 'Muh Hayatuddin Fahmi', 'default.png', '2018-08-04 09:24:46'),
(37, 5, '33415016', '$2y$10$XgfPA8dbQfHULvhEdR4HnOyHBUgTMGjeYyW84Mj9no/J9aK3gPRH.', 'Mahasiswa', 'Musa Alberto Pasha', 'default.png', '2018-08-04 09:24:46'),
(38, 5, '33415018', '$2y$10$4NE5q2EhLevHWvWQAn7qMeALpWMt8SIjtOudhJbF0SqC/Twdppa2e', 'Mahasiswa', 'Nurul Setyani', 'default.png', '2018-08-04 09:24:46'),
(39, 5, '33415019', '$2y$10$1NkfL6U4evCeq/qVqPq6VuB8HzgwyZi272qfHKoIMcC9Yix3RSTxS', 'Mahasiswa', 'Rani Kurniawati', '33415019.jpg', '2018-08-04 10:13:15'),
(40, 5, '33415020', '$2y$10$cobje2gTTgpxcaNSoB7EVu6oyGLPB9AZD8FY3YLiRbFHW./8qdkge', 'Mahasiswa', 'Rizal Dwi Prasetya', 'default.png', '2018-08-04 09:24:46'),
(41, 5, '33415021', '$2y$10$BRppAC4PrFI15fwiVhSkL.volaoEj6XjribO6y7.6ZWH50UC4uSMm', 'Mahasiswa', 'Siti Jumatun', 'default.png', '2018-08-04 09:24:46'),
(42, 5, '33415022', '$2y$10$KgNvgHIwyG/v6LrmwFsXTOI2RVfKcVpM4xPzCh387wz5WcZEoiFqq', 'Mahasiswa', 'Tatak Galih Prasetyo', 'default.png', '2018-08-04 09:24:46'),
(45, 5, '33415009', '$2y$10$TzkTHCGIUTRj.jU.cHBcx.4KgRlL36cjIdnu9u68tWcVwGXoiP0Gi', 'Mahasiswa', 'Gerry Kresna Putra', 'default.png', '2018-08-04 09:24:45'),
(46, 6, '33415101', '$2y$10$wQ1e8mZdhhCwyExww4e3eOsj4OmUIiHmHmZ0dKyvXHTPOLeTxumJm', 'Mahasiswa', 'Aditya Taufan Wiratama', 'default.png', '2018-08-09 03:51:00'),
(47, 6, '33415104', '$2y$10$GDphVRIXP3xBH412KYUFfeFtHAbO9JTczNf3UMz3zpLWG1Bsk6zSe', 'Mahasiswa', 'Aprilia Nisa Ulya', 'default.png', '2018-08-09 03:51:00'),
(48, 6, '33415105', '$2y$10$sO3qZYa0uBzfa.2GoqYck.98EYaPDUOWt4P612sIUEIjp7boG2FMu', 'Mahasiswa', 'Ariel Arkha Abimanyu', 'default.png', '2018-08-09 03:51:00'),
(49, 6, '33415106', '$2y$10$CXgUf4enfc2z7lMyMSLjheRPmLrV.UM7CyGUnLRFSsOtvWNdF7Sq2', 'Mahasiswa', 'Avin Riyan Triyanto', '33415106.jpg', '2018-08-09 03:51:00'),
(50, 6, '33415107', '$2y$10$tCUwL5kDdLRVTezM9YRD3u7.BDaTrqZW1XnAR.nnfE1TNPn5HmKE.', 'Mahasiswa', 'Azalia Charis Sekar Ayu', 'default.png', '2018-08-09 03:51:00'),
(51, 6, '33415108', '$2y$10$ShkxQHdtqE2UL3MarlLBSerHcgvbhuE.AA1klfuYtBCpY0fT1kasW', 'Mahasiswa', 'Dinar Rhamiya Pramu Zuko', 'default.png', '2018-08-09 03:51:00'),
(52, 6, '33415109', '$2y$10$yJNKeGDP9gXHOkjpJPfnGOF211HfP4jpCcxNrI/SlbBcM2xRLLws2', 'Mahasiswa', 'Enita Ayu Dhea Lestari', 'default.png', '2018-08-09 03:51:00'),
(53, 6, '33415110', '$2y$10$RkqjeFDNrLk8ZdKO2HSQj.f15.QRrYOUsGCsCgVgaKgnQ5kRfnEau', 'Mahasiswa', 'Farda Hamida', 'default.png', '2018-08-09 03:51:00'),
(54, 6, '33415111', '$2y$10$obwId8O.FemtIMfGdUn5ZeaTTP4kD3Scl2paGyVPbZ09f62gWDczq', 'Mahasiswa', 'Galuh Prima Sholawati', 'default.png', '2018-08-09 03:51:00'),
(55, 6, '33415113', '$2y$10$mYsMtXkIGA5fSOh5RFEt3eucyLEQu1ZbxuKGxOuSQs5tlyGZsXCBu', 'Mahasiswa', 'Isna Firdasari', 'default.png', '2018-08-09 03:51:00'),
(56, 6, '33415114', '$2y$10$uwU8nhjCw7F91Ta4GKVVk.rmRpHI.wwVOqngu8vWQmWQ.FFOUe9qG', 'Mahasiswa', 'Kemala Yuliana Puspawaty', 'default.png', '2018-08-09 03:51:00'),
(57, 6, '33415116', '$2y$10$fL3.kzyUcYfIBI.D4569euQi8xOAPAy1krN4X2c4HgoPBi6E35Rj.', 'Mahasiswa', 'Khumaira Anin Aliya Pahlevi', 'default.png', '2018-08-09 03:51:01'),
(58, 6, '33415117', '$2y$10$wd3.7/Byaj5rrCiTCbJ5h.c7ZLEKJB1SgI1G3rpqK.FqQTTDAu1cS', 'Mahasiswa', 'M Afif Ibadurrachman', 'default.png', '2018-08-09 03:51:01'),
(59, 6, '33415118', '$2y$10$0cf.P2noL9J8caHeRgxQu.dDidSrPrpjMwBdJH3L77OV37WW3DeLa', 'Mahasiswa', 'Muhammad Ali Masum', 'default.png', '2018-08-09 03:51:01'),
(60, 6, '33415119', '$2y$10$ytG7vIhX0xoATg/jM8VR7OeuoPcJkw79cEfrol7eDQU6dh.yH8gfa', 'Mahasiswa', 'Reinka Zaquariana Sari', 'default.png', '2018-08-09 03:51:01'),
(61, 6, '33415121', '$2y$10$mD5IjwXXXFUNP1JSsmVyuO7d11itJ3QE24P1FWITFSlt/cWtphRd6', 'Mahasiswa', 'Siti Nur Khotimah', 'default.png', '2018-08-09 03:51:01'),
(62, 6, '33415122', '$2y$10$dgWt78PtWtyx5eMnKqbIcu/ev1ltNECrabJZS6H2ow.IycTeZD3GS', 'Mahasiswa', 'Tri Meilani Utami', 'default.png', '2018-08-09 03:51:01'),
(63, 6, '33415123', '$2y$10$9ehlbBs37kfzSWcF7K8Wfe3cC9VC7sQ5zHWp8gM.XcLQR6hKjky7a', 'Mahasiswa', 'Veronica Putri Anggraini', 'default.png', '2018-08-09 03:51:01'),
(64, 6, '33415124', '$2y$10$PWy6v10EsgwdbMQ/lIx7.uZOjgWyPn5NSw8JfXbRD5VF.2B0HDg6u', 'Mahasiswa', 'Yanuar Indrawati', 'default.png', '2018-08-09 03:51:01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id_berita`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id_feedback`),
  ADD KEY `pengirim` (`pengirim`);

--
-- Indexes for table `firebase`
--
ALTER TABLE `firebase`
  ADD PRIMARY KEY (`id_firebase`);

--
-- Indexes for table `hari`
--
ALTER TABLE `hari`
  ADD PRIMARY KEY (`id_hari`);

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`id_jadwal`),
  ADD KEY `id_kelas` (`id_kelas`),
  ADD KEY `pengampu` (`pengampu`),
  ADD KEY `ruang` (`ruang`),
  ADD KEY `hari` (`hari`),
  ADD KEY `id_semester` (`id_semester`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `pengumuman`
--
ALTER TABLE `pengumuman`
  ADD PRIMARY KEY (`id_pengumuman`),
  ADD KEY `pengirim` (`pengirim`),
  ADD KEY `target` (`target`);

--
-- Indexes for table `posisi`
--
ALTER TABLE `posisi`
  ADD PRIMARY KEY (`id_posisi`),
  ADD KEY `id_user` (`id_user`,`id_ruang`),
  ADD KEY `id_ruang` (`id_ruang`);

--
-- Indexes for table `ruang`
--
ALTER TABLE `ruang`
  ADD PRIMARY KEY (`id_ruang`);

--
-- Indexes for table `semester`
--
ALTER TABLE `semester`
  ADD PRIMARY KEY (`id_semester`);

--
-- Indexes for table `surat`
--
ALTER TABLE `surat`
  ADD PRIMARY KEY (`id_surat`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `surat_tidak_mengajar`
--
ALTER TABLE `surat_tidak_mengajar`
  ADD PRIMARY KEY (`id_surat`),
  ADD KEY `pembuat` (`pengusul`,`pengganti`,`jadwal`),
  ADD KEY `pengganti` (`pengganti`),
  ADD KEY `jadwal` (`jadwal`);

--
-- Indexes for table `sync`
--
ALTER TABLE `sync`
  ADD PRIMARY KEY (`id_sync`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `id_kelas` (`id_kelas`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `id_berita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id_feedback` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `firebase`
--
ALTER TABLE `firebase`
  MODIFY `id_firebase` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `hari`
--
ALTER TABLE `hari`
  MODIFY `id_hari` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `id_jadwal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id_kelas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;
--
-- AUTO_INCREMENT for table `pengumuman`
--
ALTER TABLE `pengumuman`
  MODIFY `id_pengumuman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `posisi`
--
ALTER TABLE `posisi`
  MODIFY `id_posisi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ruang`
--
ALTER TABLE `ruang`
  MODIFY `id_ruang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;
--
-- AUTO_INCREMENT for table `semester`
--
ALTER TABLE `semester`
  MODIFY `id_semester` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `surat`
--
ALTER TABLE `surat`
  MODIFY `id_surat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `surat_tidak_mengajar`
--
ALTER TABLE `surat_tidak_mengajar`
  MODIFY `id_surat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sync`
--
ALTER TABLE `sync`
  MODIFY `id_sync` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `jadwal`
--
ALTER TABLE `jadwal`
  ADD CONSTRAINT `jadwal_ibfk_1` FOREIGN KEY (`id_kelas`) REFERENCES `kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `jadwal_ibfk_2` FOREIGN KEY (`pengampu`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `jadwal_ibfk_3` FOREIGN KEY (`ruang`) REFERENCES `ruang` (`id_ruang`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `jadwal_ibfk_4` FOREIGN KEY (`hari`) REFERENCES `hari` (`id_hari`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `jadwal_ibfk_5` FOREIGN KEY (`id_semester`) REFERENCES `semester` (`id_semester`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `pengumuman`
--
ALTER TABLE `pengumuman`
  ADD CONSTRAINT `pengumuman_ibfk_1` FOREIGN KEY (`pengirim`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pengumuman_ibfk_2` FOREIGN KEY (`target`) REFERENCES `kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `posisi`
--
ALTER TABLE `posisi`
  ADD CONSTRAINT `posisi_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `posisi_ibfk_2` FOREIGN KEY (`id_ruang`) REFERENCES `ruang` (`id_ruang`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `surat`
--
ALTER TABLE `surat`
  ADD CONSTRAINT `surat_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `surat_tidak_mengajar`
--
ALTER TABLE `surat_tidak_mengajar`
  ADD CONSTRAINT `surat_tidak_mengajar_ibfk_1` FOREIGN KEY (`pengusul`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `surat_tidak_mengajar_ibfk_2` FOREIGN KEY (`pengganti`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `surat_tidak_mengajar_ibfk_3` FOREIGN KEY (`jadwal`) REFERENCES `jadwal` (`id_jadwal`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`id_kelas`) REFERENCES `kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 14 Apr 2018 pada 14.39
-- Versi Server: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sivira`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `berita`
--

CREATE TABLE `berita` (
  `id_berita` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `isi` text NOT NULL,
  `sumber` varchar(50) NOT NULL,
  `url` varchar(255) NOT NULL,
  `upload_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `berita`
--

INSERT INTO `berita` (`id_berita`, `judul`, `thumbnail`, `isi`, `sumber`, `url`, `upload_date`) VALUES
(1, 'Buletin Ekspose Edisi Khusus Pemira | 06 April 2017', 'https://www.lpmdimensi.com/wp-content/uploads/2018/04/00012-1-e1522977928665-520x245.jpg', 'no content', 'LPM Dimensi', 'https://www.lpmdimensi.com/?p=1840', '2018-04-06 01:28:22'),
(2, 'Jadwal Tidak Jelas, Tps Keliling Belum Efektif', 'no thumbnail', 'Polines, DIMENSI (6/4) â€“ Padatnya jadwal kuliah di Politeknik Negeri Semarang (Polines) membuat panitia Pemilihan Raya (Pemira) harus menyediakan Tempat Pemungutan Suara (TPS) keliling pada saat Pemira yang telah berlangsung pada Kamis (5/4). Opsi', 'LPM Dimensi', 'https://www.lpmdimensi.com/?p=1837', '2018-04-06 00:57:12'),
(3, 'Pemira Usai, Partisipasi Mahasiswa Turun Menjadi 51%', 'no thumbnail', 'Polines, DIMENSI (06/04) &#8211; Pemiilihan raya (Pemira) yang dilaksanakan pada Kamis (05/04) Â yang dimulai dari pukul 07.30 sampai pukul 21.30 telah usai. Namun jumlah suaraÂ  yang masuk dari totalÂ  mahasiswa kurang maksimal.Dari total mahasiswa', 'LPM Dimensi', 'https://www.lpmdimensi.com/?p=1835', '2018-04-06 00:54:54'),
(4, 'Kurangnya Pengarahan Dari Panitia Saat Pemira', 'https://www.lpmdimensi.com/wp-content/uploads/2018/04/IMG_7624-520x245.jpg', 'Polines, DIMENSI &#8211; Telah dibacakan berita acara Pemilihan Raya (Pemira) pada Kamis (05/04) yang menandakan telah usainya pesta demokrasi Politeknik Negeri Semarang (Polines). Pemira yang diselenggarakan oleh Komisi Pemilihan Raya (KPR) melibatkan Panitia Pelaksana', 'LPM Dimensi', 'https://www.lpmdimensi.com/?p=1833', '2018-04-06 00:53:33'),
(5, 'Hasil Perhitungan Suara Sementara Pemira 2018', 'https://www.lpmdimensi.com/wp-content/uploads/2018/04/1522974207813-520x245.jpg', 'Polines, DIMENSI (06/04) â€“ Perhitungan hasil suara sementara Pemilihan Raya (Pemira) 2018 dilaksanakan di Hall BEM PKM, Politeknik Negeri Semarang (Polines). Berita acara dibacakan oleh ketua Komisi Pemilihan Raya (KPR) Ahmad Rijal Firdaus pada', 'LPM Dimensi', 'https://www.lpmdimensi.com/?p=1821', '2018-04-06 00:44:49'),
(6, 'Buletin Ekspose Edisi Khusus Pemira | 05 April 2018', 'https://www.lpmdimensi.com/wp-content/uploads/2018/04/header-1-520x245.jpg', 'no content', 'LPM Dimensi', 'https://www.lpmdimensi.com/?p=1806', '2018-04-05 01:43:19'),
(7, 'Mahasiswa Apatis Pengaruhi Partisipasi Pemira', 'no thumbnail', 'Oleh: Buyung Aji Saputro (Presiden Mahasiswa Polines 2017/2018) Penyunting: Nisrina dan Asyifa Mahasiswa dikatakan apatis atau tidak, harus ada dasar atau patokan yang jelas. Seorang mahasiswa yang tidak mengikuti Unit Kegiatan Mahasiswa (UKM) atau', 'LPM Dimensi', 'https://www.lpmdimensi.com/?p=1797', '2018-04-04 23:26:07'),
(8, 'Minim Informasi Pemira Di Kalangan Mahasiswa Magang', 'no thumbnail', 'Polines, DIMENSI (4/04) â€“Pemilihan Raya (Pemira) yang akan dilaksanakan pada Kamis (05/04) seharusnya dapat diikuti oleh seluruh mahasiswa Politeknik Negeri Semarang (Polines). Dalam pelaksanaan Pemiraini diharapkan menyertakan Â partisipasi dari seluruh mahasiswa Polines. Namun karena', 'LPM Dimensi', 'https://www.lpmdimensi.com/?p=1795', '2018-04-04 23:24:15'),
(9, 'Kekurangan Calon, Kpr Terapkan Sistem Ambang Batas', 'https://www.lpmdimensi.com/wp-content/uploads/2018/04/galfot-pemira-520x245.jpg', 'Polines, DIMENSI (5/4) â€“ Pemilihan Raya (Pemira) tahun 2018 memberlakukan sistem threshold (ambang batas) sebesar 10 persen Â dari total suara sah kepada calon Badan Perwakilan Mahasiswa (BPM) agar dapat terpilih. Sistem ini ditetapkan setelah', 'LPM Dimensi', 'https://www.lpmdimensi.com/?p=1793', '2018-04-04 23:21:40'),
(10, 'Buletin Ekspose Edisi Khusus Pemira | 04 April 2018', 'https://www.lpmdimensi.com/wp-content/uploads/2018/04/header-520x245.jpg', 'no content', 'LPM Dimensi', 'https://www.lpmdimensi.com/?p=1785', '2018-04-04 06:39:08'),
(11, 'Poltekkes Bengkulu Lakukan Kunjungan Benchmarking Ke Polines', 'https://www.polines.ac.id/id/images/FotoArtikel/intro_225x150/20180410-225.jpg', 'Kampus Politeknik Negeri Semarang (Polines) kembali menjadi tujuan kunjungan. Kali ini, Politeknik Kesehatan (Poltekkes) Bengkulu mengadakan kunjungan dalam kegiatan benchmarking dalam bidang penjaminan mutu ke kampus Polines Tembalang.', 'Polines.ac.id', 'https://www.polines.ac.id/id/index.php/berita/779-poltekkes-bengkulu-lakukan-kunjungan-benchmarking-ke-polines', '2018-04-10 21:09:31'),
(12, 'Mulai Hari Ini, Polines Buka Pendaftaran Magister (s2) Terapan T. Telekomunikasi', 'https://www.polines.ac.id/id/images/FotoArtikel/intro_225x150/20180403-2.jpg', 'Politeknik Negeri Semarang (Polines) secara resmi membuka pendaftaran mahasiswa baru program Magister (S2) Terapan Teknik Telekomunikasi tahun akademik 2018/2019. Program magister pertama di Polines ini, dibuka dalam rangka menghasilan lulusan yang mampu mengembangkan prototype sistem telekomunikasi dan informasi terkini dengan pendekatan inter atau multidisipliner hingga menghasilkan karya yang teruji, diakui secara nasional atau internasional dalam bentuk publikasi ilmiah tingkat nasional maupun internasional.', 'Polines.ac.id', 'https://www.polines.ac.id/id/index.php/berita/775-mulai-hari-ini-polines-buka-pendaftaran-magister-s2-terapan-teknik-telekomunikasi', '2018-04-03 08:46:49'),
(13, 'Direktur Polines Lantik 6 Pejabat Baru', 'https://www.polines.ac.id/id/images/FotoArtikel/intro_225x150/20180403-3.jpg', 'Direktur Politeknik Negeri Semarang (Polines), Ir. Supriyadi, MT melantik dan mengambil sumpah jabatan 6 (enam) pejabat baru di Ruang Sidang Direktur, kampus Polines Tembalang, Senin (2/4). Acara pelantikan dihadiri oleh anggota senat akademik, jajaran pimpinan jurusan, bagian, pusat, unit serta pengurus Dharma Wanita Polines.', 'Polines.ac.id', 'https://www.polines.ac.id/id/index.php/berita/777-direktur-polines-lantik-6-pejabat-baru', '2018-04-02 13:21:04'),
(14, ' Rintis Kerjasama, Bupati Mentawai Kunjungi Polines', 'https://www.polines.ac.id/id/images/FotoArtikel/intro_225x150/20180406.jpg', 'Politeknik Negeri Semarang (Polines) menerima kunjungan Bupati Kepulauan Mentawai, Yudas Sabaggalet beserta rombongan, Kamis (5/4). Kunjungan tersebut diterima langsung Direktur Polines, Ir. Supriyadi, MT di Ruang Sidang Direktur, Kampus Tembalang. Ikut serta menerima kunjungan adalah Wakil Direktur dan jajaran pimpinan Polines.', 'Polines.ac.id', 'https://www.polines.ac.id/id/index.php/berita/778-rintis-kerjasama-bupati-mentawai-kunjungi-polines', '2018-04-05 15:35:00'),
(15, 'Workshop Kampus Siaga Bencana (ksb) Dan Fire Rescue', 'https://www.polines.ac.id/id/images/FotoArtikel/intro_225x150/20180403-225.png', 'Workshop Kampus Siaga Bencana dan Fire Rescue merupakan kegiatan yang bertujuan untuk menumbuhkan kesiapsiagaan menuju insan kampus yang siap dan tanggap dalam penanggulangan bencana. Kegiatan ini diselenggarakan pada hari Sabtu-Minggu, 24-25 Maret 2018.', 'Polines.ac.id', 'https://www.polines.ac.id/id/index.php/berita/774-siaga-bencana', '2018-04-03 08:35:03'),
(16, 'Tim Abhijaya Sipondra Sabet Juara 1 Lomba Maket Nasional ', 'https://www.polines.ac.id/id/images/FotoArtikel/intro_225x150/20180319-225.jpg', 'Tradisi juara kembali diperoleh tim Politeknik Negri Semarang (Polines). Tim Abhijaya Sipondra (Sipil Polines Duta Juara) yang beranggotakanÂ  mahasiswa dari Jurusan Teknik Sipil Politeknik Negeri Semarang (Polines) berhasil meraih juara 1 Lomba Maket Nasional: Green Building Innovation Contes (GBIC) 2018 yang digelar di kampus Fakultas Teknik,Â  Universitas Islam Sultan Agung (Unissula), Kaligawe, Semarang, Sabtu, (17/3).Â ', 'Polines.ac.id', 'https://www.polines.ac.id/id/index.php/berita/773-tim-abhijaya-sipondra-sabet-juara-1-lomba-maket-nasional', '2018-03-19 14:57:15');

-- --------------------------------------------------------

--
-- Struktur dari tabel `feedback`
--

CREATE TABLE `feedback` (
  `id_feedback` int(11) NOT NULL,
  `pengirim` int(11) NOT NULL,
  `feedback` text NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `feedback`
--

INSERT INTO `feedback` (`id_feedback`, `pengirim`, `feedback`, `tanggal`) VALUES
(1, 1, 'Mantab Gan', '2018-04-14 12:38:31'),
(2, 4, 'asdasdasdas\nasdasdadA\nSDASD\nASDASDadadsA\nSDA', '2018-04-14 12:38:47');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hari`
--

CREATE TABLE `hari` (
  `id_hari` int(11) NOT NULL,
  `hari` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `hari`
--

INSERT INTO `hari` (`id_hari`, `hari`) VALUES
(1, 'Senin'),
(2, 'Selasa'),
(3, 'Rabu'),
(4, 'Kamis'),
(5, 'Jumat');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jadwal`
--

CREATE TABLE `jadwal` (
  `id_jadwal` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `matakuliah` text NOT NULL,
  `pengampu` int(11) NOT NULL,
  `ruang` int(11) NOT NULL,
  `hari` int(11) NOT NULL,
  `jam_mulai` time NOT NULL,
  `jam_ke_mulai` int(11) NOT NULL,
  `jam_selesai` time NOT NULL,
  `jam_ke_selesai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jadwal`
--

INSERT INTO `jadwal` (`id_jadwal`, `id_kelas`, `matakuliah`, `pengampu`, `ruang`, `hari`, `jam_mulai`, `jam_ke_mulai`, `jam_selesai`, `jam_ke_selesai`) VALUES
(1, 1, 'Desain Grafis Lanjut (TP)', 6, 3, 1, '10:20:00', 0, '15:30:00', 0),
(2, 1, 'Komunikasi Data (TP)', 15, 5, 1, '17:30:00', 0, '19:30:00', 0),
(3, 1, 'Pendidikan Agama', 7, 5, 2, '10:20:00', 0, '11:50:00', 0),
(4, 1, 'Pemrograman Visual (TP)', 8, 4, 2, '14:00:00', 0, '17:30:00', 0),
(5, 1, 'Sistem Basis Data Dasar (T)', 8, 5, 3, '12:30:00', 0, '14:00:00', 0),
(6, 1, 'Statistika (T)', 14, 5, 3, '14:00:00', 0, '15:30:00', 0),
(7, 1, 'Komunikasi Data (TP)', 15, 5, 3, '16:30:00', 0, '19:30:00', 0),
(8, 1, 'PBO (TP)', 4, 3, 4, '10:20:00', 0, '15:30:00', 0),
(9, 1, 'Pancasila', 16, 5, 4, '16:00:00', 0, '17:30:00', 0),
(10, 1, 'Sistem Basis Data Dasar (P)', 8, 2, 5, '07:00:00', 0, '10:20:00', 0),
(11, 1, 'Metode Numerik', 23, 5, 5, '10:20:00', 0, '11:50:00', 0),
(12, 2, 'Sistem Basis Data Dasar (TP)', 8, 4, 1, '07:00:00', 0, '11:50:00', 0),
(13, 2, 'Pancasila', 16, 5, 1, '14:00:00', 0, '15:30:00', 0),
(14, 2, 'Pendidikan Agama', 7, 5, 2, '08:30:00', 0, '10:20:00', 0),
(15, 2, 'Pemrograman Visual (TP)', 8, 4, 2, '10:20:00', 0, '14:00:00', 0),
(16, 2, 'Komunikasi Data (TP)', 15, 5, 2, '17:30:00', 0, '19:30:00', 0),
(17, 2, 'PBO (T)', 4, 5, 3, '07:00:00', 0, '08:30:00', 0),
(18, 2, 'Komunikasi Data (TP)', 15, 2, 3, '08:30:00', 0, '14:00:00', 0),
(19, 2, 'PBO (P)', 4, 3, 4, '07:00:00', 0, '10:20:00', 0),
(20, 2, 'Statistika', 14, 5, 4, '10:20:00', 0, '11:50:00', 0),
(21, 2, 'Desain Grafis Lanjut (Tp)', 6, 5, 5, '07:00:00', 0, '11:50:00', 0),
(22, 2, 'Metode Numerik', 23, 5, 5, '13:30:00', 0, '15:00:00', 0),
(23, 3, 'Bahasa Indonesia', 17, 5, 1, '08:30:00', 0, '10:20:00', 0),
(24, 3, 'Bahasa Inggris II', 21, 6, 1, '12:30:00', 0, '14:00:00', 0),
(25, 3, 'Perancangan Sistem Informasi (TP)', 4, 3, 1, '14:00:00', 0, '14:45:00', 0),
(26, 3, 'Animasi dan Desain Grafis (T)', 18, 5, 2, '12:30:00', 0, '14:00:00', 0),
(27, 3, 'Mikroprosesor & Antarmuka (T)', 19, 2, 2, '14:00:00', 0, '15:30:00', 0),
(28, 3, 'Mikroprosesor & Antarmuka (P)', 19, 2, 2, '16:45:00', 0, '20:15:00', 0),
(29, 3, 'Pemrograman Web Berbais Framework (TP)', 18, 4, 3, '14:00:00', 0, '19:30:00', 0),
(30, 3, 'Pemrograman Basis Data', 20, 4, 4, '12:30:00', 0, '17:30:00', 0),
(31, 3, 'Animasi dan Desain Grafis (P)', 18, 3, 4, '17:30:00', 0, '20:15:00', 0),
(32, 3, 'Jaringan Komputer II (TP)', 14, 2, 5, '13:30:00', 0, '18:15:00', 0),
(33, 4, 'Bahasa Indonesia', 17, 5, 1, '17:00:00', 0, '18:30:00', 0),
(34, 4, 'Bahasa Inggris II', 21, 6, 1, '08:30:00', 0, '10:20:00', 0),
(35, 4, 'Perancangan Sistem Informasi (T)', 4, 5, 1, '10:20:00', 0, '11:50:00', 0),
(36, 4, 'Mikroprosesor & Antarmuka (T)', 19, 5, 1, '14:45:00', 0, '16:45:00', 0),
(37, 4, 'Perancangan Sistem Informasi (P)', 4, 3, 2, '07:00:00', 0, '09:15:00', 0),
(38, 4, 'Pemrograman Basis Data', 20, 3, 2, '09:35:00', 0, '15:30:00', 0),
(39, 4, 'Animasi dan Desain Grafis (TP)', 18, 3, 3, '07:00:00', 0, '11:05:00', 0),
(40, 4, 'Mikroprosesor & Antarmuka (P)', 19, 3, 3, '11:05:00', 0, '15:30:00', 0),
(41, 4, 'Jaringan Komputer II (TP)', 14, 2, 4, '07:00:00', 0, '11:50:00', 0),
(42, 4, 'Pemrogaman Web Berbasis Framework (TP)', 5, 4, 5, '07:00:00', 0, '11:50:00', 0),
(43, 5, 'Sistem Manajemen Mutu', 15, 5, 1, '12:30:00', 0, '14:00:00', 0),
(44, 5, 'Pemrograman Aplikasi Mobile', 5, 4, 1, '16:00:00', 0, '21:00:00', 0),
(45, 5, 'Sistem Manajemen Mutu', 15, 5, 2, '07:00:00', 0, '08:30:00', 0),
(46, 5, 'Bahasa Inggris IV', 21, 6, 2, '08:30:00', 0, '10:20:00', 0),
(47, 5, 'Man. Internetworking & Router', 20, 2, 2, '12:30:00', 0, '15:30:00', 0),
(48, 5, 'Man. Internetworking & Router', 6, 2, 3, '14:00:00', 0, '18:15:00', 0),
(49, 5, 'Sistem Informasi Enterprise', 22, 4, 4, '07:00:00', 0, '11:50:00', 0),
(50, 5, 'K3, Hukum, & Etika Profesi', 15, 5, 4, '12:30:00', 0, '14:00:00', 0),
(51, 5, 'Tugas Akhir', 24, 1, 5, '07:00:00', 0, '15:00:00', 0),
(52, 6, 'Man. Internetworking & Router', 20, 2, 1, '07:00:00', 0, '10:20:00', 0),
(53, 6, 'Sistem Manajemen Mutu', 15, 6, 1, '10:20:00', 0, '11:50:00', 0),
(54, 6, 'Sistem Informasi Enterprise (T)', 23, 5, 1, '12:30:00', 0, '14:00:00', 0),
(55, 6, 'Bahasa Inggris IV', 21, 6, 2, '07:00:00', 0, '08:30:00', 0),
(56, 6, 'Sistem Informasi Enterprise (P)', 22, 2, 2, '08:30:00', 0, '11:50:00', 0),
(57, 6, 'Pemrograman Aplikasi Mobile', 5, 4, 3, '07:00:00', 0, '13:15:00', 0),
(58, 6, 'K3, Hukum, & Etika Profesi', 15, 5, 4, '09:35:00', 0, '11:05:00', 0),
(59, 6, 'Man. Internetworking & Router', 6, 2, 4, '12:30:00', 0, '15:30:00', 0),
(60, 6, 'Tugas Akhir', 24, 1, 5, '07:00:00', 0, '15:00:00', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelas`
--

CREATE TABLE `kelas` (
  `id_kelas` int(11) NOT NULL,
  `kelas` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `kelas`) VALUES
(1, 'IK-1A'),
(2, 'IK-1B'),
(3, 'IK-2A'),
(4, 'IK-2B'),
(5, 'IK-3A'),
(6, 'IK-3B'),
(99, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengumuman`
--

CREATE TABLE `pengumuman` (
  `id_pengumuman` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `pengumuman` text NOT NULL,
  `target` int(11) NOT NULL,
  `pengirim` int(11) NOT NULL,
  `upload_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengumuman`
--

INSERT INTO `pengumuman` (`id_pengumuman`, `judul`, `pengumuman`, `target`, `pengirim`, `upload_date`) VALUES
(1, 'Pengumuman Kompensasi', 'Untuk Semua Mahasiswa Prodi TI,\r\nsdadadad\r\nasdasdasdasdasd\r\nasdasd\r\nasdasdasda', 99, 4, '2018-04-07 00:00:00'),
(2, 'Ketua Kelas IK-3A', 'Ketua Kelas IK-3A Diharapkan menemui Pak Prayit', 5, 5, '2018-04-07 00:00:00'),
(3, 'Pengumuman Untuk Semua Mahasiswa Prodi TI', 'sadkjaskjdhkasd\r\nasdas\r\nd\r\nasd\r\nas\r\n\r\n\r\nasd\r\nad\r\nasd\r\nas\r\ndas\r\ndasdad', 99, 4, '2018-04-06 00:00:00'),
(4, 'Ini Judul Yaaa..', 'Isi Pengumuman..\r\ndasdasdasd\r\nasd\r\na\r\nsd\r\nasd\r\na\r\nsd\r\nas\r\nd\r\nasd', 99, 5, '2018-04-04 00:00:00'),
(5, 'Ini Judul Yaaa..', 'Isi Pengumuman..\r\ndasdasdasd\r\nasd\r\na\r\nsd\r\nasd\r\na\r\nsd\r\nas\r\nd\r\nasd', 99, 5, '2018-04-04 00:00:00'),
(6, 'Ini Judul Yaaa..', 'Isi Pengumuman..\r\ndasdasdasd\r\nasd\r\na\r\nsd\r\nasd\r\na\r\nsd\r\nas\r\nd\r\nasd', 99, 5, '2018-04-04 00:00:00'),
(7, 'Ini Judul Yaaa..', 'Isi Pengumuman..\r\ndasdasdasd\r\nasd\r\na\r\nsd\r\nasd\r\na\r\nsd\r\nas\r\nd\r\nasd', 99, 5, '2018-04-04 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `posisi`
--

CREATE TABLE `posisi` (
  `id_posisi` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `last_sync` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `posisi`
--

INSERT INTO `posisi` (`id_posisi`, `id_user`, `id_ruang`, `last_sync`) VALUES
(1, 4, 1, '2018-04-07 12:42:23'),
(2, 5, 1, '2018-04-07 12:44:59'),
(3, 6, 1, '2018-04-07 13:38:37');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ruang`
--

CREATE TABLE `ruang` (
  `id_ruang` int(11) NOT NULL,
  `ruang` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ruang`
--

INSERT INTO `ruang` (`id_ruang`, `ruang`) VALUES
(1, 'Ruang Dosen'),
(2, 'Lab. Jaringan Komputer'),
(3, 'Lab. Multimedia'),
(4, 'Lab. Pemrograman'),
(5, 'SB I 04'),
(6, 'MST'),
(99, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `surat`
--

CREATE TABLE `surat` (
  `id_surat` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `jenis` varchar(20) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `tanggal_surat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `surat`
--

INSERT INTO `surat` (`id_surat`, `id_user`, `jenis`, `judul`, `foto`, `tanggal_surat`, `tanggal`) VALUES
(1, 4, 'Surat Undangan', 'Rapat Koordinasi', 'example.jpg', '2018-04-07 05:19:42', '2018-04-07 05:19:42'),
(2, 4, 'Surat Undangan', 'Rapat Tahunan', '', '2018-04-07 05:19:42', '2018-04-07 05:19:42');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `nomor_induk` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `tipe` enum('Dosen','Mahasiswa','Ketua Program Studi') NOT NULL,
  `nama` varchar(255) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `last_sync` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `id_kelas`, `nomor_induk`, `password`, `tipe`, `nama`, `foto`, `last_sync`) VALUES
(1, 5, '33415023', '$2y$10$pmVz9JH62fbKyrvX63nQ6ufSlX/5kpgDW1C.20jPFv1jbV82ZwWJC', 'Mahasiswa', 'Thio Van Agusti', '33415023.jpg', '2018-04-13 05:21:20'),
(2, 6, '33415120', '$2y$10$JocdiLH4UeUUniA.khk/C.fa.obRvcZnttIdCyqaFygkCm43RcXki', 'Mahasiswa', 'Septyan Ajid Nugroho', '33415120.jpg', '2018-04-08 10:45:18'),
(4, 99, '001', '$2y$10$JocdiLH4UeUUniA.khk/C.fa.obRvcZnttIdCyqaFygkCm43RcXki', 'Ketua Program Studi', 'SUKAMTO, S.Kom, M.T.', '001.jpg', '2018-04-08 10:45:26'),
(5, 99, '002', '$2y$10$JocdiLH4UeUUniA.khk/C.fa.obRvcZnttIdCyqaFygkCm43RcXki', 'Dosen', 'PRAYITNO, SST., M.T.', '002.jpg', '2018-04-08 10:45:35'),
(6, 99, '198404202015041003', '$2y$10$JocdiLH4UeUUniA.khk/C.fa.obRvcZnttIdCyqaFygkCm43RcXki', 'Dosen', 'LILIEK TRIYONO, S.T., M.Kom.', 'default.png', '2018-04-08 10:44:20'),
(7, 99, '197206102000031001', '$2y$10$JocdiLH4UeUUniA.khk/C.fa.obRvcZnttIdCyqaFygkCm43RcXki', 'Dosen', 'KHAMAMI, S.Ag., M.M.', 'default.png', '2018-04-08 10:44:20'),
(8, 99, '197501302001121001', '$2y$10$JocdiLH4UeUUniA.khk/C.fa.obRvcZnttIdCyqaFygkCm43RcXki', 'Dosen', 'SLAMET HANDOKO, S. Kom, M. Kom.', 'default.png', '2018-04-08 10:44:20'),
(14, 99, '196008221988031001', '$2y$10$JocdiLH4UeUUniA.khk/C.fa.obRvcZnttIdCyqaFygkCm43RcXki', 'Dosen', 'PARSUMO RAHARDJO, Drs, M. Kom.', 'default.png', '2018-04-08 10:44:20'),
(15, 99, '196810252000121001', '$2y$10$JocdiLH4UeUUniA.khk/C.fa.obRvcZnttIdCyqaFygkCm43RcXki', 'Dosen', 'TRI RAHARJO YUDANTORO, S.Kom., M.Kom.', 'default.png', '2018-04-08 10:44:20'),
(16, 99, '197307082005011001', '$2y$10$JocdiLH4UeUUniA.khk/C.fa.obRvcZnttIdCyqaFygkCm43RcXki', 'Dosen', 'TAUFIQ YULIANTO, S.H, M.H.', 'default.png', '2018-04-08 10:44:20'),
(17, 99, '196107101988112001', '$2y$10$JocdiLH4UeUUniA.khk/C.fa.obRvcZnttIdCyqaFygkCm43RcXki', 'Dosen', 'NETTY NURDIYANI, Dra, M. Hum.', 'default.png', '2018-04-08 10:44:20'),
(18, 99, '197610032003121002', '$2y$10$JocdiLH4UeUUniA.khk/C.fa.obRvcZnttIdCyqaFygkCm43RcXki', 'Dosen', 'BUDI SUYANTO, S.T, M. Eng.', 'default.png', '2018-04-08 10:44:20'),
(19, 99, '197704012005011001', '$2y$10$JocdiLH4UeUUniA.khk/C.fa.obRvcZnttIdCyqaFygkCm43RcXki', 'Dosen', 'WAHYU SULISTIYO, S.T., M.Kom.', 'default.png', '2018-04-08 10:44:20'),
(20, 99, '197403112000121001', '$2y$10$JocdiLH4UeUUniA.khk/C.fa.obRvcZnttIdCyqaFygkCm43RcXki', 'Dosen', 'MARDIYONO, S.Kom, M. Sc.', 'default.png', '2018-04-08 10:44:20'),
(21, 99, '195901191988031001', '$2y$10$JocdiLH4UeUUniA.khk/C.fa.obRvcZnttIdCyqaFygkCm43RcXki', 'Dosen', 'SASONGKO, Drs, M. Hum.', 'default.png', '2018-04-08 10:44:20'),
(22, 99, '197711192008012013', '$2y$10$JocdiLH4UeUUniA.khk/C.fa.obRvcZnttIdCyqaFygkCm43RcXki', 'Dosen', 'IDHAWATI HESTININGSIH, S.Kom, M.Kom.', 'default.png', '2018-04-08 10:44:20'),
(23, 99, '197912272003122001', '$2y$10$JocdiLH4UeUUniA.khk/C.fa.obRvcZnttIdCyqaFygkCm43RcXki', 'Dosen', 'ISWANTI, S.Si, M.Sc.', 'default.png', '2018-04-08 10:44:20'),
(24, 99, '99', '', 'Dosen', 'Tim Dosen', 'default.png', '2018-04-08 10:44:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id_berita`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id_feedback`),
  ADD KEY `id_user` (`pengirim`);

--
-- Indexes for table `hari`
--
ALTER TABLE `hari`
  ADD PRIMARY KEY (`id_hari`);

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`id_jadwal`),
  ADD KEY `id_kelas` (`id_kelas`),
  ADD KEY `pengampu` (`pengampu`),
  ADD KEY `ruang` (`ruang`),
  ADD KEY `hari` (`hari`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `pengumuman`
--
ALTER TABLE `pengumuman`
  ADD PRIMARY KEY (`id_pengumuman`),
  ADD KEY `pengirim` (`pengirim`),
  ADD KEY `target` (`target`);

--
-- Indexes for table `posisi`
--
ALTER TABLE `posisi`
  ADD PRIMARY KEY (`id_posisi`),
  ADD KEY `id_user` (`id_user`,`id_ruang`),
  ADD KEY `id_ruang` (`id_ruang`);

--
-- Indexes for table `ruang`
--
ALTER TABLE `ruang`
  ADD PRIMARY KEY (`id_ruang`);

--
-- Indexes for table `surat`
--
ALTER TABLE `surat`
  ADD PRIMARY KEY (`id_surat`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `id_kelas` (`id_kelas`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `id_berita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id_feedback` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `hari`
--
ALTER TABLE `hari`
  MODIFY `id_hari` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `id_jadwal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id_kelas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;
--
-- AUTO_INCREMENT for table `pengumuman`
--
ALTER TABLE `pengumuman`
  MODIFY `id_pengumuman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `posisi`
--
ALTER TABLE `posisi`
  MODIFY `id_posisi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ruang`
--
ALTER TABLE `ruang`
  MODIFY `id_ruang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;
--
-- AUTO_INCREMENT for table `surat`
--
ALTER TABLE `surat`
  MODIFY `id_surat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `feedback`
--
ALTER TABLE `feedback`
  ADD CONSTRAINT `feedback_ibfk_1` FOREIGN KEY (`pengirim`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `jadwal`
--
ALTER TABLE `jadwal`
  ADD CONSTRAINT `jadwal_ibfk_1` FOREIGN KEY (`id_kelas`) REFERENCES `kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `jadwal_ibfk_2` FOREIGN KEY (`pengampu`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `jadwal_ibfk_3` FOREIGN KEY (`ruang`) REFERENCES `ruang` (`id_ruang`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `jadwal_ibfk_4` FOREIGN KEY (`hari`) REFERENCES `hari` (`id_hari`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `pengumuman`
--
ALTER TABLE `pengumuman`
  ADD CONSTRAINT `pengumuman_ibfk_1` FOREIGN KEY (`pengirim`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pengumuman_ibfk_2` FOREIGN KEY (`target`) REFERENCES `kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `posisi`
--
ALTER TABLE `posisi`
  ADD CONSTRAINT `posisi_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `posisi_ibfk_2` FOREIGN KEY (`id_ruang`) REFERENCES `ruang` (`id_ruang`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `surat`
--
ALTER TABLE `surat`
  ADD CONSTRAINT `surat_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`id_kelas`) REFERENCES `kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

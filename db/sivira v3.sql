-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 22 Mar 2018 pada 23.55
-- Versi Server: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sivira`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `berita`
--

CREATE TABLE `berita` (
  `id_berita` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `isi` text NOT NULL,
  `sumber` varchar(50) NOT NULL,
  `url` varchar(255) NOT NULL,
  `upload_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `berita`
--

INSERT INTO `berita` (`id_berita`, `judul`, `thumbnail`, `isi`, `sumber`, `url`, `upload_date`) VALUES
(1, 'Bidikmisi Akan Dicabut Jika Tak Tepat Sasaran', 'https://www.lpmdimensi.com/wp-content/uploads/2018/03/2ce289ae-660d-4f93-bec5-699344763efd-520x245.jpg', 'Polines, DIMENSI (1/3) â€“ Direktorat Jenderal Pembelajaran dan Kemahasiswaan (Ditjen Belmawa) Kementerian Riset, Teknologi, dan Perguruan Tinggi (Kemenristek Dikti) akan mencabut bidikmisi yang tak tepat sasaran dan akan menindak tegas Politeknik di Indonesia yang', 'LPM Dimensi', 'https://www.lpmdimensi.com/?p=1721', '2018-03-01 23:25:17'),
(2, 'Pemangkasan Jalur Pendaftaran Calon Anggota Bpm Tuai Pro Kontra', 'no thumbnail', 'Polines, DIMENSI (7/2) â€“ Ketetapan Komisi Pemilihan Raya (KPR) Polines 2018 Nomor 004/TAP/KPR/2018 pada bab 1 pasal 1 nomor 2Â  menyatakan jika jalur pendaftaran calon anggota Badan Perwakilan Mahasiswa (BPM) dalam Pemilihan Raya (Pemira)', 'LPM Dimensi', 'https://www.lpmdimensi.com/?p=1696', '2018-02-06 22:55:27'),
(3, 'Kkl Dari Hal Penting Hingga Jadi Ajang Hiburan', 'no thumbnail', 'Polines, DIMENSI (09/01) â€“ Pelaksanaan Kunjungan Kerja Lapangan (KKL) yang dilaksanakan setiap tahun untuk mahasiswa bertujuan sebagai penunjang kegiatan belajar mahasiswa di luar kampus melalui kegiatan kunjungan industri. Namun, kegiatan ini lebih mengagendakan perjalanan', 'LPM Dimensi', 'https://www.lpmdimensi.com/?p=1687', '2018-01-23 23:01:18'),
(4, 'Permasalahan Kuota Tambahan Ppa, Institusi Akui Salah', 'https://www.lpmdimensi.com/wp-content/uploads/2018/01/76-520x245.jpg', 'Polines, DIMENSI (15/01) â€“ Berkaitan mengenai masalah ketidakpastian jumlah mahasiswa tambahan penerima program beasiswa Peningkatan Prestasi Akademik (PPA) yang mulanya sebanyak 181 mahasiswa menjadi 30 mahasiswa, maka mahasiswa melakukan audiensi terbuka dengan pihak institusi', 'LPM Dimensi', 'https://www.lpmdimensi.com/?p=1682', '2018-01-16 13:49:07'),
(5, 'Buletin Ekspose Edisi Magang', 'no thumbnail', 'no content', 'LPM Dimensi', 'https://www.lpmdimensi.com/?p=1678', '2018-01-15 10:22:21'),
(6, 'Hadirnya Prodi Baru Di Jurusan Teknik Mesin', 'no thumbnail', 'Polines, DIMENSI (19/12) â€“ Jurusan Teknik Mesin Politeknik Negeri Semarang (Polines) membuka program studi (prodi) baru Sarjana Terapan Teknologi Rekayasa Pembangkit Energi (PE) berdasarkan Surat Keputusan Menteri Riset, Teknologi, dan Pendidikan Tinggi (Menristekdikti) Republik', 'LPM Dimensi', 'https://www.lpmdimensi.com/?p=1653', '2018-01-03 04:54:42'),
(7, 'Ekspresi Seni, Hiburan Ditengah Kepenatan Perkuliahan', 'https://www.lpmdimensi.com/wp-content/uploads/2017/12/kkk-520x245.jpg', 'Polines, DIMENSI (21/12) â€“ Pada tahun ini UKM KonSeP menyelenggarakan kembali acara Ekspresi Seni dengan mengusung tema â€œZona Nyamanâ€. Acara yang berlangsung pada Selasa (19/12)dan dimulai dari pukul 18.30 WIB di Kantin Tata Niaga', 'LPM Dimensi', 'https://www.lpmdimensi.com/?p=1635', '2017-12-21 23:19:37'),
(8, 'Pembangunan Ulang Jembatan, Tuai Banyak Pertanyaan', 'https://www.lpmdimensi.com/wp-content/uploads/2017/12/61796-520x245.jpg', 'Polines, DIMENSI (19/12) &#8211; Jembatan lantai tiga parkiran Tata Niaga (TN) Politeknik Negeri Semarang telah selesai dibangun ulang. Pembangunan ini dilakukan selama 30 hari dan telah dinyatakan selesai pada Jumat (15/12) lalu. Pembangunan ulang', 'LPM Dimensi', 'https://www.lpmdimensi.com/?p=1626', '2017-12-21 22:47:20'),
(9, 'Ktm Terlambat, Kegiatan Mahasiswa Terhambat', 'no thumbnail', 'Polines, DIMENSI (19/12) â€“ Sudah empat bulan menjadi mahasiswa di Politeknik Negeri Semarang (Polines), mahasiswa angkatan 2017 belum mendapatkan Kartu Tanda Mahasiswa (KTM). Sehingga banyak dari mahasiswa yang Â menanyakan keterlambatan pembagian KTM tersebut. Kensi', 'LPM Dimensi', 'https://www.lpmdimensi.com/?p=1630', '2017-12-21 11:23:28'),
(10, 'Terpilihnya Wakil Direktur Baru Polines', 'https://www.lpmdimensi.com/wp-content/uploads/2017/12/7286-520x245.jpg', 'Penyerahan berkas berita acara pemilihan wakil direktur Politeknik Negeri Semarang periode 2018-2022 dari Ketua Senat kepada Direktur di ruang sidang direktur pada Jumat (8/12) lalu. Dok. pribadi &nbsp; Polines, DIMENSI (12/12) â€“ Pada Jumat', 'LPM Dimensi', 'https://www.lpmdimensi.com/?p=1621', '2017-12-13 04:58:53'),
(11, 'Direktur Polines Lantik 4 Wakil Direktur Baru Masa Bakti 2018-2022', 'https://www.polines.ac.id/id/images/FotoArtikel/intro_225x150/20180309-225-pelantikan-wadir.jpg', 'Direktur Politeknik Negeri Semarang (Polines), Ir. Supriyadi, MT melantik dan mengambil sumpah jabatan 4 (empat) Wakil Direktur (Wadir) Polines masa bakti 2018-2022 di Ruang Serba Guna, kampus Polines Tembalang, Jumat (9/3). Acara pelantikan dihadiri oleh para mantan pimpinan Politeknik, anggota senat akademik, jajaran pimpinan jurusan, bagian, pusat, unit, organisasi kemahasiswaan, mitra kerja, Muspika Tembalang serta pengurus Dharma Wanita Polines.', 'Polines.ac.id', 'https://www.polines.ac.id/id/index.php/berita/765-direktur-polines-lantik-4-wakil-direktur-baru-masa-bakti-2018-2022', '2018-03-09 20:00:27'),
(12, 'Dari Tahun Ke Tahun, Pendaftar Jalur Spa Polines Terus Meningkat', 'https://www.polines.ac.id/id/images/FotoArtikel/intro_225x150/20180304-225-suasana_tes-spa.jpg', 'Peminat masuk mahasiswa baru jalur undangan melalui sistem Seleksi Potensi Akademik (SPA) di Politeknik Negeri Semarang (Polines) terus meningkat dari tahun ke tahun. Tercatat hingga penutupan pendaftaran pada pekan lalu, jumlah pendaftar jalur SPA tahun 2018 ini mencapai 3.779 orang atau meningkat 13,6% dibandingkan dengan jumlah pendaftar tahun sebelumnya.', 'Polines.ac.id', 'https://www.polines.ac.id/id/index.php/berita/762-dari-tahun-ke-tahun-pendaftar-jalur-spa-polines-terus-meningkat', '2018-03-04 16:20:32'),
(13, 'Politeknik Ilmu Pelayaran Makassar Studi Banding Ke Polines', 'https://www.polines.ac.id/id/images/FotoArtikel/intro_225x150/20180301-2.jpg', 'Kampus Politeknik Negeri Semarang (Polines) kembali menjadi tujuan untuk studi banding. Kali ini, Politeknik Ilmu Pelayaran (PIP) Makassar mengadakan kunjungan studi banding dalam tata kelola institusi ke kampus Polines Tembalang. Rombongan tamu dari PIP Makassar dipimpin oleh Hasiah, ST, MAP, dan diterima oleh Wakil Direktur Bidang Akademik Akhmad Jamaah, ST, MEng.', 'Polines.ac.id', 'https://www.polines.ac.id/id/index.php/berita/757-pip-makassar-studi-banding-ke-polines', '2018-03-01 16:34:08'),
(14, 'Pegawai Polines Antusias Ikuti â€œngisi Barengâ€ Spt Tahunan', 'https://www.polines.ac.id/id/images/FotoArtikel/intro_225x150/20180308.jpg', 'Kantor Pelayanan Pajak (KPP) Pratama Semarang Candisari menyelenggarakan sosialisasi perpajakan berupa â€œNgisi Barengâ€ SPT Tahunan Pajak Penghasilan (PPh) pasal 21 tahun pajak 2017 secara online E-Filing di kampus Politeknik Negeri Semarang (Polines), Rabu (7/3).', 'Polines.ac.id', 'https://www.polines.ac.id/id/index.php/berita/763-pegawai-polines-antusias-ikuti-ngisi-bareng-spt-tahunan', '2018-03-07 09:16:23'),
(15, 'Polines Buka Kelas Kerjasama Dengan Pt Pln (persero)', 'https://www.polines.ac.id/id/images/FotoArtikel/intro_225x150/20180301-3.jpg', 'Politeknik Negeri Semarang (Polines) membuka penerimaan mahasiswa baru jalur kelas kerjasama PT PLN (Persero), pada tahun akademik 2018/2019. Jalur tersebut diperuntukkan bagi para siswa lulusan SMA jurusan IPA, atau SMK jurusan kelistrikan.', 'Polines.ac.id', 'https://www.polines.ac.id/id/index.php/berita/760-polines-buka-kelas-kerjasama-dengan-pln', '2018-03-02 08:25:42'),
(16, 'Dua Rombongan Tamu Dari Polmed Studi Banding Ke Polines', 'https://www.polines.ac.id/id/images/FotoArtikel/intro_225x150/2018030-1.jpg', 'Politeknik Negeri Semarang (Polines), belum lama ini menerima kunjungan studi banding dari Politeknik Negeri Medan (Polmed) dalam dua rombongan. Dalam kunjungan pertama, yang hadir adalah jajaran Senat Polmed dipimpin oleh Drs. Suharjono, MT (Ketua Senat) besertaÂ  jajaran senat Komisi Kerjasama.', 'Polines.ac.id', 'https://www.polines.ac.id/id/index.php/berita/756-dua-rombongan-tamu-dari-polmed-studi-banding-ke-polines', '2018-03-01 16:24:20');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hari`
--

CREATE TABLE `hari` (
  `id_hari` int(11) NOT NULL,
  `hari` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `hari`
--

INSERT INTO `hari` (`id_hari`, `hari`) VALUES
(0, 'Minggu'),
(1, 'Senin'),
(2, 'Selasa'),
(3, 'Rabu'),
(4, 'Kamis'),
(5, 'Jumat'),
(6, 'Sabtu');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jadwal`
--

CREATE TABLE `jadwal` (
  `id_jadwal` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `matakuliah` text NOT NULL,
  `pengampu` int(11) NOT NULL,
  `ruang` int(11) NOT NULL,
  `hari` int(11) NOT NULL,
  `jam_mulai` time NOT NULL,
  `jam_selesai` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jadwal`
--

INSERT INTO `jadwal` (`id_jadwal`, `id_kelas`, `matakuliah`, `pengampu`, `ruang`, `hari`, `jam_mulai`, `jam_selesai`) VALUES
(1, 1, 'Algoritma Dasar', 4, 1, 1, '07:00:00', '09:30:00'),
(2, 4, 'Komunikasi Data', 5, 1, 1, '12:00:00', '16:00:00'),
(3, 5, 'K3 Dan Etika Profesi', 4, 2, 1, '07:00:00', '08:30:00'),
(4, 5, 'Sistem Manajemen Mutu', 4, 1, 1, '12:30:00', '14:00:00'),
(6, 5, 'Sistem Informasi Enterprise', 5, 2, 4, '07:00:00', '12:00:00'),
(7, 5, 'Kewirausahaan', 4, 1, 3, '12:00:00', '14:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelas`
--

CREATE TABLE `kelas` (
  `id_kelas` int(11) NOT NULL,
  `kelas` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `kelas`) VALUES
(1, 'IK-1A'),
(2, 'IK-1B'),
(3, 'IK-2A'),
(4, 'IK-2B'),
(5, 'IK-3A'),
(6, 'IK-3B'),
(99, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengumuman`
--

CREATE TABLE `pengumuman` (
  `id_pengumuman` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `pengumuman` text NOT NULL,
  `target` int(11) NOT NULL,
  `pengirim` int(11) NOT NULL,
  `upload_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengumuman`
--

INSERT INTO `pengumuman` (`id_pengumuman`, `judul`, `pengumuman`, `target`, `pengirim`, `upload_date`) VALUES
(1, 'Untuk Semua', 'Semua\n1aswdasdasdadsfsdfsdfsdfsdfsdfsdfsdfsdffsdfdsfdsfdsfdsfdsfsdfdsfdsfsdfdsfsdf\nas\nd\nasd\nas\nd\nad', 99, 4, '2018-05-09 17:00:00'),
(2, 'Untuk IK-1B', 'Mahasiswa IK-1B\nasdasd\nsssad\nasd', 2, 1, '2018-03-22 16:00:00'),
(3, 'Untuk IK-3A', 'Mahasiswa IK-3A\nsada\n36\n5\n\n\n\nff', 5, 4, '2018-03-13 19:00:00'),
(4, 'Pengumuman Uji Ulang', 'sadasd\r\nasd\r\nas\r\nd\r\nas\r\nd\r\nasd\r\nas\r\ndasdasdasdasdasdasdasdasdasdas', 99, 4, '2018-03-02 22:00:00'),
(5, 'Pengumumman Surat Bebas Kompen', 'sdaasd1313asd1as\r\nd13sad12\r\nas3d123as\r\n2d13as2d13asd1', 99, 5, '2018-03-10 00:00:00'),
(6, 'Pengumumman Surat Bebas Kompen', 'sdaasd1313asd1as\r\nd13sad12\r\nas3d123as\r\n2d13as2d13asd1', 99, 5, '2018-03-10 00:00:00'),
(7, 'Pengumuman Uji Ulang', 'sadasd\r\nasd\r\nas\r\nd\r\nas\r\nd\r\nasd\r\nas\r\ndasdasdasdasdasdasdasdasdasdas', 99, 4, '2018-03-02 22:00:00'),
(8, 'Pengumumman Surat Bebas Kompen', 'sdaasd1313asd1as\r\nd13sad12\r\nas3d123as\r\n2d13as2d13asd1', 99, 5, '2018-03-10 00:00:00'),
(9, 'Pengumumman Surat Bebas Kompen', 'sdaasd1313asd1as\r\nd13sad12\r\nas3d123as\r\n2d13as2d13asd1', 99, 5, '2018-03-10 00:00:00'),
(10, 'Pengumuman Uji Ulang', 'sadasd\r\nasd\r\nas\r\nd\r\nas\r\nd\r\nasd\r\nas\r\ndasdasdasdasdasdasdasdasdasdas', 99, 4, '2018-03-02 22:00:00'),
(11, 'Pengumumman Surat Bebas Kompen', 'sdaasd1313asd1as\r\nd13sad12\r\nas3d123as\r\n2d13as2d13asd1', 99, 5, '2018-03-10 00:00:00'),
(12, 'Pengumumman Surat Bebas Kompen', 'sdaasd1313asd1as\r\nd13sad12\r\nas3d123as\r\n2d13as2d13asd1', 99, 5, '2018-03-10 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `posisi`
--

CREATE TABLE `posisi` (
  `id_posisi` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `last_sync` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `posisi`
--

INSERT INTO `posisi` (`id_posisi`, `id_user`, `id_ruang`, `last_sync`) VALUES
(2, 1, 2, '2018-02-16 00:09:38'),
(4, 2, 2, '2018-02-16 00:18:33');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ruang`
--

CREATE TABLE `ruang` (
  `id_ruang` int(11) NOT NULL,
  `ruang` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ruang`
--

INSERT INTO `ruang` (`id_ruang`, `ruang`) VALUES
(1, 'Ruang Dosen'),
(2, 'Jaringan Komputer');

-- --------------------------------------------------------

--
-- Struktur dari tabel `surat`
--

CREATE TABLE `surat` (
  `id_surat` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `jenis` varchar(20) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `foto` varchar(20) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `nomor_induk` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `tipe` enum('Dosen','Mahasiswa','Ketua Program Studi') NOT NULL,
  `nama` varchar(255) NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `last_sync` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `id_kelas`, `nomor_induk`, `password`, `tipe`, `nama`, `no_hp`, `foto`, `last_sync`) VALUES
(1, 5, '33415023', '$2y$10$7mDOgjvx1G9oQdM0KSQyb.8o1pp1z5bMo3JWfSQYkJDhEbdMB3jWy', 'Mahasiswa', 'Thio Van Agusti', '08700', 'https://cdn.business2community.com/wp-content/uploads/2014/04/profile-picture-300x300.jpg', '2018-03-10 07:54:44'),
(2, 6, '33415120', '$2y$10$rmTD9pVa3AWMk9fmdZskHOyk49asB.p8Y9eFYxSy2vtHBB8h0jI0C', 'Mahasiswa', 'Septyan Ajid Nugroho', '085600000', 'http://mhalabs.org/wp-content/uploads/upme/1451456913_brodie.jpg', '2018-03-10 07:55:10'),
(4, 99, '001', '$2y$10$3r7ZAo8xLU3OaTXzga8wEuRqJWH.qfcszqP.xYWN8DfN1PgrHr6L2', 'Ketua Program Studi', 'Sukamto, S.Kom, M.T.', '009', 'http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg', '2018-03-16 10:31:15'),
(5, 99, '002', '$2y$10$F8zQ2MFc2IGOK8fDYGKE5es5bWvs9VlVghikvKGfQdLcTBxrliCCS', 'Dosen', 'Prayitno, SST., M.T.', '009', 'http://www.assurances-dauphin.fr/wp-content/uploads/2015/05/guiy_dauphin.jpg', '2018-03-13 15:58:50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id_berita`);

--
-- Indexes for table `hari`
--
ALTER TABLE `hari`
  ADD PRIMARY KEY (`id_hari`);

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`id_jadwal`),
  ADD KEY `id_kelas` (`id_kelas`),
  ADD KEY `pengampu` (`pengampu`),
  ADD KEY `ruang` (`ruang`),
  ADD KEY `hari` (`hari`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `pengumuman`
--
ALTER TABLE `pengumuman`
  ADD PRIMARY KEY (`id_pengumuman`),
  ADD KEY `pengirim` (`pengirim`),
  ADD KEY `target` (`target`);

--
-- Indexes for table `posisi`
--
ALTER TABLE `posisi`
  ADD PRIMARY KEY (`id_posisi`),
  ADD KEY `id_user` (`id_user`,`id_ruang`),
  ADD KEY `id_ruang` (`id_ruang`);

--
-- Indexes for table `ruang`
--
ALTER TABLE `ruang`
  ADD PRIMARY KEY (`id_ruang`);

--
-- Indexes for table `surat`
--
ALTER TABLE `surat`
  ADD PRIMARY KEY (`id_surat`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `id_kelas` (`id_kelas`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `id_berita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `hari`
--
ALTER TABLE `hari`
  MODIFY `id_hari` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `id_jadwal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id_kelas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;
--
-- AUTO_INCREMENT for table `pengumuman`
--
ALTER TABLE `pengumuman`
  MODIFY `id_pengumuman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `posisi`
--
ALTER TABLE `posisi`
  MODIFY `id_posisi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ruang`
--
ALTER TABLE `ruang`
  MODIFY `id_ruang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `surat`
--
ALTER TABLE `surat`
  MODIFY `id_surat` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `jadwal`
--
ALTER TABLE `jadwal`
  ADD CONSTRAINT `jadwal_ibfk_1` FOREIGN KEY (`id_kelas`) REFERENCES `kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `jadwal_ibfk_2` FOREIGN KEY (`pengampu`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `jadwal_ibfk_3` FOREIGN KEY (`ruang`) REFERENCES `ruang` (`id_ruang`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `jadwal_ibfk_4` FOREIGN KEY (`hari`) REFERENCES `hari` (`id_hari`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `pengumuman`
--
ALTER TABLE `pengumuman`
  ADD CONSTRAINT `pengumuman_ibfk_1` FOREIGN KEY (`pengirim`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pengumuman_ibfk_2` FOREIGN KEY (`target`) REFERENCES `kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `posisi`
--
ALTER TABLE `posisi`
  ADD CONSTRAINT `posisi_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `posisi_ibfk_2` FOREIGN KEY (`id_ruang`) REFERENCES `ruang` (`id_ruang`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `surat`
--
ALTER TABLE `surat`
  ADD CONSTRAINT `surat_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`id_kelas`) REFERENCES `kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

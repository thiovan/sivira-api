<?php
include 'config.php';
include 'authentication.php';

//check if request not empty
if (!empty($_SERVER['HTTP_AUTH']) && !empty($_GET['target'])) {

	//get request value
	$header = $_SERVER['HTTP_AUTH'];
	$target = $_GET['target'];

	//check auth token
	if (check_auth($header, $AUTH['TOKEN'])) {

		$sql = "SELECT pengumuman.id_pengumuman, pengumuman.pengumuman, pengumuman.judul, pengumuman.target, user.nama, user.foto, pengumuman.upload_date
				FROM pengumuman
				INNER JOIN user ON pengumuman.pengirim=user.id_user
				WHERE target = $target
				ORDER BY upload_date DESC";
		$result = mysqli_query($conn, $sql);

		$json = array();
		if (mysqli_num_rows($result) > 0){

			while ($row = mysqli_fetch_assoc($result)) {
				$row_array['id_pengumuman'] = $row['id_pengumuman'];
				$row_array['pengumuman'] = $row['pengumuman'];
				$row_array['judul'] = $row['judul'];
				$row_array['target'] = $row['target'];
				$row_array['nama'] = $row['nama'];
				$row_array['foto'] = $DIR['USER_IMAGE'].$row['foto'];
				$date_formated = date_create($row['upload_date']);
				$row_array['upload_date'] = date_format($date_formated,"H:i  d F Y");
				array_push($json,$row_array);
			}
		}else{

			$row_array['id_pengumuman'] = "";
			$row_array['pengumuman'] = "";
			$row_array['judul'] = "";
			$row_array['target'] = "";
			$row_array['nama'] = "";
			$row_array['foto'] = "";
			$row_array['upload_date'] = "";
			array_push($json,$row_array);
		}
	} else {

		$json['success'] = 0;
		$json['message'] = 'Authentication Token Mismatch';
	}

	echo json_encode(array('data' => $json));
	
}
?>
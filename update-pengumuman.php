<?php
include 'config.php';
include 'authentication.php';

//check if request not empty
if (!empty($_SERVER['HTTP_AUTH']) && !empty($_POST['judul']) && !empty($_POST['pengumuman']) && !empty($_POST['id_pengumuman'])) {

	//get request value
	$header = $_SERVER['HTTP_AUTH'];
	$judul = $_POST['judul'];
	$pengumuman = $_POST['pengumuman'];
	$id_pengumuman = $_POST['id_pengumuman'];

	//check auth token
	if (check_auth($header, $AUTH['TOKEN'])) {

		$sql = "UPDATE pengumuman SET judul='$judul', pengumuman='$pengumuman', upload_date=CURRENT_TIMESTAMP WHERE id_pengumuman='$id_pengumuman'";
		$result = mysqli_query($conn, $sql);

		if (mysqli_query($conn, $sql)) {

			$json['success'] = 1;
			$json['message'] = 'Pengumuman Berhasil Di Update';
		} else {

		    $json['success'] = 0;
			$json['message'] = 'Pengumuman Gagal Di Update, Mohon Coba Lagi';
		}
	} else {

		$json['success'] = 0;
		$json['message'] = 'Authentication Token Mismatch';
	}

	echo json_encode($json);
	
}
?>
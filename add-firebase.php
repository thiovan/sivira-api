<?php
include 'config.php';
include 'authentication.php';

//check if request not empty
if (!empty($_SERVER['HTTP_AUTH']) && !empty($_POST['id_user']) && !empty($_POST['token']) && !empty($_POST['topic'])) {

	//get request value
	$header = $_SERVER['HTTP_AUTH'];
	$id_user = $_POST['id_user'];
	$token = $_POST['token'];
	$topic = $_POST['topic'];

	//check auth token
	if (check_auth($header, $AUTH['TOKEN'])) {

		$sql = "SELECT * FROM firebase WHERE id_user='$id_user'";
		$result = mysqli_query($conn, $sql);

		if (mysqli_num_rows($result) > 0) {

			$sql = "UPDATE firebase SET token='$token', topic='$topic' WHERE id_user='$id_user'";
			if (mysqli_query($conn, $sql)) {

				$json['success'] = 1;
				$json['message'] = 'Token Firebase Berhasil Diupdate';
			} else {

				$json['success'] = 1;
				$json['message'] = 'Token Firebase Gagal Diupdate';
			}
		} else {

			$sql = "INSERT INTO firebase VALUES ('', '$id_user', '$token', '$topic')";
			if (mysqli_query($conn, $sql)) {

				$json['success'] = 1;
				$json['message'] = 'Token Firebase Berhasil Ditambahkan';
			} else {

				$json['success'] = 1;
				$json['message'] = 'Token Firebase Gagal Ditambahkan';
			}
		}
	} else {

		$json['success'] = 0;
		$json['message'] = 'Authentication Token Mismatch';
	}

	echo json_encode($json);
	
}
?>
<?php
include 'config.php';
include 'authentication.php';

//check if request not empty
if (!empty($_SERVER['HTTP_AUTH'])) {

	//get request value
	$header = $_SERVER['HTTP_AUTH'];

	//check auth token
	if (check_auth($header, $AUTH['TOKEN'])) {

		$sql = "SELECT * FROM sync";
		$result = mysqli_query($conn, $sql);

		if (mysqli_num_rows($result) > 0){

			$json = array();
			while ($row = mysqli_fetch_assoc($result)) {

				$row_array['nama_tabel'] = $row['nama_tabel'];
				$row_array['last_modified'] = $row['last_modified'];
				array_push($json,$row_array);
			}
		} else {

			$json['success'] = 0;
			$json['message'] = 'Sync Tidak Ditemukan';
		}

	} else {

		$json['success'] = 0;
		$json['message'] = 'Authentication Token Mismatch';
	}

	echo json_encode(array('data' => $json));
	
}
?>
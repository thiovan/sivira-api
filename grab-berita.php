<?php
include 'config.php';
include 'authentication.php';
error_reporting(0);

//check if database is initialized
$sql = "SELECT * FROM berita";
$result = mysqli_query($conn, $sql);
if (mysqli_num_rows($result) < 16){
	//initialize database for news data
	for($i = 0; $i<16; $i++) {
	    $sql="INSERT INTO berita (id_berita, judul, isi, sumber, url) VALUES ('','','','','')";
	    $result = mysqli_query($conn, $sql);
	    if (!$result) {
			die('Invalid query: ' . mysqli_error($conn));
	    }
	}
}

//grab news from www.lpmdimensi.com
$url = 'https://www.lpmdimensi.com/?cat=32';
$content = file_get_contents($url);
$judul_arr = array();
$isi_arr = array();
$url_arr = array();
$thumbnail_arr = array();
$date_arr = array();

//grab news title 
$grab = explode( '<h2 class="post-title entry-title">' , $content );
//check if parameter is valid
if (empty($grab[1])) {
	die("Site: ".$url."<br>Grabber Parameter Mismatch, Please Configure With New Parameter<br>");
}
for($i=1; $i<11; $i++){
	$judul = explode("</h2>" , $grab[$i] );
	$judul = explode('title="' , $judul[0] );
	$judul = explode('">' , $judul[1] );
	array_push($judul_arr, ucwords(strtolower($judul[0])));
}

//grab news content
$grab = explode( '<div class="entry excerpt entry-summary">' , $content );
//check if parameter is valid
if (empty($grab[1])) {
	die("Site: ".$url."<br>Grabber Parameter Mismatch, Please Configure With New Parameter<br>");
}
for($i=1; $i<11; $i++){
	$isi = explode("</div>" , $grab[$i] );
	$isi = explode('<p>' , $isi[0] );
	$isi = explode('&#46;&#46;&#46;</p>' , $isi[1] );
	if($isi[0] == ""){
		array_push($isi_arr, "no content");
	} else {
		array_push($isi_arr, strip_tags($isi[0]));
	}
}

//grab news url
$grab = explode( '<h2 class="post-title entry-title">' , $content );
//check if parameter is valid
if (empty($grab[1])) {
	die("Site: ".$url."<br>Grabber Parameter Mismatch, Please Configure With New Parameter<br>");
}
for($i=1; $i<11; $i++){
	$urll = explode("</h2>" , $grab[$i] );
	$urll = explode('href="' , $urll[0] );
	$urll = explode('" rel' , $urll[1] );
	array_push($url_arr, $urll[0]);
}

//grab news thumbnail
$grab = explode( '<div class="post-thumbnail">' , $content );
//check if parameter is valid
if (empty($grab[1])) {
	die("Site: ".$url."<br>Grabber Parameter Mismatch, Please Configure With New Parameter<br>");
}
for($i=1; $i<11; $i++){
	$thumbnail = explode("</div>" , $grab[$i] );
	$thumbnail = explode('<img width="520" height="245" src="' , $thumbnail[0] );
	$thumbnail = explode('" class=' , $thumbnail[1] );
	if($thumbnail[0] == ""){
		array_push($thumbnail_arr, "no thumbnail");
	} else {
		array_push($thumbnail_arr, $thumbnail[0]);
	}
}

//grab news date
$grab = explode( '<p class="post-date">' , $content );
//check if parameter is valid
if (empty($grab[1])) {
	die("Site: ".$url."<br>Grabber Parameter Mismatch, Please Configure With New Parameter<br>");
}
for($i=1; $i<11; $i++){
	$date = explode("</p>" , $grab[$i] );
	$date = explode('<time class="published updated" datetime="' , $date[0] );
	$date = explode('">' , $date[1] );
	if($date[0] == ""){
		array_push($date_arr, "no date");
	} else {
		$date_formated = date_create($date[0]);
		array_push($date_arr, date_format($date_formated,"Y-m-d H:i:s"));
	}
	
}

//store news data to database
for($i = 0; $i<10; $i++) {
	$judul_clear = str_replace("'", "", $judul_arr[$i]);
	$isi_clear = str_replace("'", "", $isi_arr[$i]);
	$url_clear = str_replace("'", "", $url_arr[$i]);
	$thumbnail_clear = str_replace("'", "", $thumbnail_arr[$i]);
	$date_clear = str_replace("'", "", $date_arr[$i]);
	$sql = "UPDATE berita SET judul='$judul_clear', thumbnail='$thumbnail_clear', isi='$isi_clear', sumber='LPM Dimensi', url='$url_clear', upload_date='$date_clear' WHERE id_berita=$i+1";
    $result = mysqli_query($conn, $sql);
    if (!$result) {
		die('Invalid query: ' . mysqli_error($conn));
    }
}
echo "Success store news data from https://www.lpmdimensi.com/?cat=32 <br>";
//end of grab news from www.lpmdimensi.com



//grab news from https://www.polines.ac.id/id/
$url = 'https://www.polines.ac.id/id/';
$content = file_get_contents($url);
$judul_arr = array();
$isi_arr = array();
$url_arr = array();
$thumbnail_arr = array();
$date_arr = array();

//grab news title
$grab = explode( '<h2 class="article-title" itemprop="headline">' , $content );
//check if parameter is valid
if (empty($grab[1])) {
	die("Site: ".$url."<br>Grabber Parameter Mismatch, Please Configure With New Parameter<br>");
}
for($i=1; $i<7; $i++){
	$judul = explode("</h2>" , $grab[$i] );
	$judul = explode('title="' , $judul[0] );
	$judul = explode('">' , $judul[1] );
	array_push($judul_arr, ucwords(strtolower($judul[0])));
}

//grab news content
$grab = explode( '<section class="article-intro clearfix" itemprop="articleBody">' , $content );
//check if parameter is valid
if (empty($grab[1])) {
	die("Site: ".$url."<br>Grabber Parameter Mismatch, Please Configure With New Parameter<br>");
}
for($i=1; $i<7; $i++){
	$isi = explode("</section>" , $grab[$i] );
	$isi = explode('<p style="text-align: justify;">' , $isi[0] );
	$isi = explode('</p>' , $isi[1] );
	if($isi[0] == ""){
		array_push($isi_arr, "no content");
	} else {
		array_push($isi_arr, strip_tags($isi[0]));
	}
}

//grab news url
$grab = explode( '<h2 class="article-title" itemprop="headline">' , $content );
//check if parameter is valid
if (empty($grab[1])) {
	die("Site: ".$url."<br>Grabber Parameter Mismatch, Please Configure With New Parameter<br>");
}
for($i=1; $i<7; $i++){
	$urll = explode("</h2>" , $grab[$i] );
	$urll = explode('href="' , $urll[0] );
	$urll = explode('" itemprop' , $urll[1] );
	array_push($url_arr, "https://www.polines.ac.id".$urll[0]);
}

//grab news thumbnail
$grab = explode( '<div class="pull-left item-image article-image article-image-intro">' , $content );
//check if parameter is valid
if (empty($grab[1])) {
	die("Site: ".$url."<br>Grabber Parameter Mismatch, Please Configure With New Parameter<br>");
}
for($i=1; $i<7; $i++){
	$thumbnail = explode("</div>" , $grab[$i] );
	$thumbnail = explode('data-src="' , $thumbnail[0] );
	$thumbnail = explode('" data-jchll=' , $thumbnail[1] );
	if($thumbnail[0] == ""){
		array_push($thumbnail_arr, "no thumbnail");
	} else {
		array_push($thumbnail_arr, "https://www.polines.ac.id" . $thumbnail[0]);
	}
	
}

//grab news date
$grab = explode( '<dd class="published hasTooltip" title="Ditayangkan: ">' , $content );
//check if parameter is valid
if (empty($grab[1])) {
	die("Site: ".$url."<br>Grabber Parameter Mismatch, Please Configure With New Parameter<br>");
}
for($i=1; $i<7; $i++){
	$date = explode("</dd>" , $grab[$i] );
	$date = explode('<time datetime="' , $date[0] );
	$date = explode('" itemprop=' , $date[1] );
	if($date[0] == ""){
		array_push($date_arr, "no date");
	} else {
		$date_formated = date_create($date[0]);
		array_push($date_arr, date_format($date_formated,"Y-m-d H:i:s"));
	}
	
}

//store news data to database
for($i = 0; $i<6; $i++) {
	$judul_clear = str_replace("'", "", $judul_arr[$i]);
	$isi_clear = str_replace("'", "", $isi_arr[$i]);
	$url_clear = str_replace("'", "", $url_arr[$i]);
	$thumbnail_clear = str_replace("'", "", $thumbnail_arr[$i]);
	$date_clear = str_replace("'", "", $date_arr[$i]);
	$sql = "UPDATE berita SET judul='$judul_clear', thumbnail='$thumbnail_clear', isi='$isi_clear', sumber='Polines.ac.id', url='$url_clear', upload_date='$date_clear' WHERE id_berita=$i+11";
    $result = mysqli_query($conn, $sql);
    if (!$result) {
		die('Invalid query: ' . mysqli_error($conn));
    }
}
echo "Success store news data from https://www.polines.ac.id/id/ <br>";
//grab grab news from https://www.polines.ac.id/id/

echo date('H:i d-m-Y');
echo "<br>================================================================<br>";
?>
<?php
include 'config.php';
include 'authentication.php';

//check if request not empty
if (!empty($_SERVER['HTTP_AUTH']) && !empty($_POST['id_jadwal'])) {

	//get request value
	$header = $_SERVER['HTTP_AUTH'];
	$id_jadwal = $_POST['id_jadwal'];
	$pertemuan = $_POST['pertemuan'];
	$materi = $_POST['materi'];
	$status = $_POST['status'];

	//check auth token
	if (check_auth($header, $AUTH['TOKEN'])) {

		$sql = "SELECT * FROM kontrol_pbm WHERE id_jadwal='$id_jadwal'";
		$result = mysqli_query($conn, $sql);
		if (mysqli_num_rows($result) > 0) {

			$sql = "UPDATE kontrol_pbm SET pertemuan='$pertemuan', materi='$materi', status='$status' WHERE id_jadwal='$id_jadwal'";
			$result = mysqli_query($conn, $sql);
			if (mysqli_query($conn, $sql)) {

				$json['success'] = 1;
				$json['message'] = 'Data Berhasil Di Update';
			} else {

			    $json['success'] = 0;
				$json['message'] = 'Data Gagal Di Update, Mohon Coba Lagi';
			}
		} else {
			$pertemuan = "1$$2$$3$$4$$5$$6$$7$$8$$9$$10$$11$$12$$13$$14$$15$$16$$17$$18";
			$materi = "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$";
			$status = "0$$0$$0$$0$$0$$0$$0$$0$$0$$0$$0$$0$$0$$0$$0$$0$$0$$0";
			$sql = "INSERT INTO kontrol_pbm (id_kontrol, id_jadwal, pertemuan, materi, status) VALUES ('', '$id_jadwal', '$pertemuan', '$materi', '$status')";
			if (mysqli_query($conn, $sql)) {

			    $json['success'] = 1;
				$json['message'] = 'Data Berhasil Di Update';
			} else {

			    $json['success'] = 0;
				$json['message'] = 'Data Gagal Di Update, Mohon Coba Lagi';
			}
		}
	} else {

		$json['success'] = 0;
		$json['message'] = 'Authentication Token Mismatch';
	}

	echo json_encode($json);
	
}
?>
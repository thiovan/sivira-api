<?php
include 'config.php';
include 'authentication.php';

//check if request not empty
if (!empty($_SERVER['HTTP_AUTH']) && !empty($_POST['password']) && !empty($_POST['nomor_induk'])) {

	//get request value
	$header = $_SERVER['HTTP_AUTH'];
	$password = $_POST['password'];
	$nomor_induk = $_POST['nomor_induk'];

	//check auth token
	if (check_auth($header, $AUTH['TOKEN'])) {

		$hashed_password = password_hash($password, PASSWORD_DEFAULT);
		$sql = "UPDATE user SET password='$hashed_password' WHERE nomor_induk='$nomor_induk'";
		$result = mysqli_query($conn, $sql);

		if (mysqli_query($conn, $sql)) {

			$json['success'] = 1;
			$json['message'] = 'Data Berhasil Di Update';
		} else {

		    $json['success'] = 0;
			$json['message'] = 'Data Gagal Di Update, Mohon Coba Lagi';
		}
	} else {

		$json['success'] = 0;
		$json['message'] = 'Authentication Token Mismatch';
	}

	echo json_encode($json);
	
}
?>
<?php
include 'config.php';
include 'authentication.php';

//check if request not empty
if (!empty($_SERVER['HTTP_AUTH']) && !empty($_GET['id_jadwal'])) {

	//get request value
	$header = $_SERVER['HTTP_AUTH'];
	$id_jadwal = $_GET['id_jadwal'];

	//check auth token
	if (check_auth($header, $AUTH['TOKEN'])) {

		$sql = "SELECT * FROM kontrol_pbm WHERE id_jadwal='$id_jadwal' LIMIT 1";
		$result = mysqli_query($conn, $sql);

		if (mysqli_num_rows($result) > 0){

			$pertemuan_arr = array();
			$materi_arr = array();
			$status_arr = array();
			while ($row = mysqli_fetch_assoc($result)) {
				$id_kontrol = $row['id_kontrol'];
				$id_jadwal = $row['id_jadwal'];
				$pertemuan_arr = explode('$$', $row['pertemuan']);
				unset($pertemuan_arr[count($pertemuan_arr)]);
				$materi_arr = explode('$$', $row['materi']);
				unset($materi_arr[count($materi_arr)]);
				$status_arr = explode('$$', $row['status']);
				unset($status_arr[count($status_arr)]);
				echo json_encode(array(
					'id_kontrol' => $id_kontrol,
					'id_jadwal' => $id_jadwal,
					'pertemuan' => $pertemuan_arr,
					'materi' => $materi_arr,
					'status' => $status_arr
					));
			}
		}else{

			echo json_encode(array(
				'id_kontrol' => '',
				'id_jadwal' => '',
				'pertemuan' => array(),
				'materi' => array(),
				'status' => array()
				));
		}
	} else {

		$json['success'] = 0;
		$json['message'] = 'Authentication Token Mismatch';
	}

	
	
}
?>
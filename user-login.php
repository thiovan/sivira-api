<?php
include 'config.php';
include 'authentication.php';

if (!empty($_SERVER['HTTP_AUTH']) && !empty($_POST['nomor_induk']) && !empty($_POST['password'])) {
	//get request content
	$header = $_SERVER['HTTP_AUTH'];
	$nomor_induk = $_POST['nomor_induk'];
	$password = $_POST['password'];

	if (check_auth($header, $AUTH['TOKEN'])) {
		
		$sql = "SELECT user.password, user.id_user, kelas.kelas, user.nomor_induk, user.tipe, user.nama, user.foto, user.last_sync 
				FROM user
				INNER JOIN kelas ON user.id_kelas=kelas.id_kelas
				WHERE nomor_induk='$nomor_induk'
				LIMIT 1";
		$result = mysqli_query($conn, $sql);

		if (mysqli_num_rows($result) > 0){

			$row = mysqli_fetch_assoc($result);
			if ($row['tipe']=="Mahasiswa" && $row['kelas']=="") {
				
				$json['success'] = 0;
		    	$json['message'] = 'Akses Ditolak';
			} else {

				if (password_verify($password, $row['password'])) {
					
					$json['success'] = 1;
			    	$json['message'] = 'Login Berhasil';
			    	$json['id_user'] = $row['id_user'];
			    	$json['kelas'] = $row['kelas'];
			    	$json['nomor_induk'] = $row['nomor_induk'];
			    	$json['tipe'] = $row['tipe'];
			    	$json['nama'] = $row['nama'];
			    	$json['foto'] = $DIR['USER_IMAGE'].$row['foto'];
			    	$json['last_sync'] = $row['last_sync'];
				}else{

					$json['success'] = 0;
			    	$json['message'] = 'Password Yang Anda Masukkan Salah';
				}
			}
		} else {

			$json['success'] = 0;
		    $json['message'] = 'Nomor Induk Tidak Terdaftar';
		}
	} else {

		$json['success'] = 0;
		$json['message'] = 'Authentication Token Mismatch';
	}

	echo json_encode($json);

}
?>
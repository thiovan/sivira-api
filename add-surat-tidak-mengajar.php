<?php
include 'config.php';
include 'authentication.php';
include 'firebase.php';

//check if request not empty
if (!empty($_SERVER['HTTP_AUTH']) && !empty($_POST['pengusul']) && !empty($_POST['pengganti']) && !empty($_POST['jadwal']) && !empty($_POST['alasan']) && !empty($_POST['keterangan'])) {

	//get request value
	$header = $_SERVER['HTTP_AUTH'];
	$pengusul = $_POST['pengusul'];
	$pengganti = $_POST['pengganti'];
	$jadwal = $_POST['jadwal'];
	$alasan = $_POST['alasan'];
	$keterangan = $_POST['keterangan'];
	$status = $_POST['status'];

	//check auth token
	if (check_auth($header, $AUTH['TOKEN'])) {
		
		$sql = "INSERT INTO surat_tidak_mengajar VALUES ('', '$pengusul', '$pengganti', '$jadwal', '$alasan', '$keterangan', '$status', CURRENT_TIMESTAMP)";
		if (mysqli_query($conn, $sql)) {

			$sql = "SELECT token FROM firebase WHERE id_user='$pengganti' LIMIT 1";
			$result = mysqli_query($conn, $sql);
			$row = mysqli_fetch_assoc($result);
			$key = $AUTH['FIREBASE_API_KEY'];
			$targetNotifikasi = $row['token'];
			$title = "Surat Tidak Bisa Mengajar";
			$judul_stbm = "Permintaan Konfirmasi Dosen Pengganti";
			$isi_stbm = "";
			sendNotification($key, $targetNotifikasi, "", $title, $judul_stbm, $isi_stbm);

		    $json['success'] = 1;
			$json['message'] = 'Surat Tidak Mengajar Berhasil Dikirim';
		} else {

		    $json['success'] = 0;
			$json['message'] = 'Surat Tidak Mengajar Gagal Dikirim, Mohon Coba Lagi';
		}
	} else {

		$json['success'] = 0;
		$json['message'] = 'Authentication Token Mismatch';
	}

	echo json_encode($json);
	
} else {
	print_r($_POST);
}
?>
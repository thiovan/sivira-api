<?php
include 'config.php';
include 'authentication.php';

//check if request not empty
if (!empty($_SERVER['HTTP_AUTH']) && !empty($_GET['id_user']) && !empty($_GET['kelas'])) {

	//get request value
	$header = $_SERVER['HTTP_AUTH'];
	$id_user = $_GET['id_user'];
	$kelas = $_GET['kelas'];
	
	$arr_id_hari = array(
				"Monday" => 1,
				"Tuesday" => 2,
				"Wednesday" => 3,
				"Thursday" => 4,
				"Friday" => 5,
				"Saturday" => 6,
				"Sunday" => 7
		);
	$id_hari = $arr_id_hari[date('l')];

	//check auth token
	if (check_auth($header, $AUTH['TOKEN'])) {

		$json = array();
		$json2 = array();
		$json3 = array();

		//get matakuliah selanjutnya
		$sql = "SELECT matakuliah, jam_mulai, hari.hari
				FROM jadwal
				INNER JOIN kelas ON jadwal.id_kelas = kelas.id_kelas
				INNER JOIN hari ON jadwal.hari = hari.id_hari
				WHERE kelas.kelas = '$kelas' AND jadwal.hari = '$id_hari' AND jam_mulai > CURRENT_TIME
				LIMIT 1";
		$result = mysqli_query($conn, $sql);

		if (mysqli_num_rows($result) > 0){

			while ($row = mysqli_fetch_assoc($result)) {
				$row_array['matakuliah'] = $row['matakuliah'];
				$row_array['jam_mulai'] = substr($row['jam_mulai'], 0, 5);
				$row_array['hari'] = $row['hari'];
				
			}
		}else{

			if ($id_hari == "5" || $id_hari == "6" || $id_hari == "7") {
				$next_id_hari = 1;
			} else {
				$next_id_hari = $id_hari + 1;
			}
			$sql = "SELECT matakuliah, jam_mulai, hari.hari
					FROM jadwal
					INNER JOIN kelas ON jadwal.id_kelas = kelas.id_kelas
					INNER JOIN hari ON jadwal.hari = hari.id_hari
					WHERE kelas.kelas = '$kelas' AND jadwal.hari = '$next_id_hari'
					LIMIT 1";
			$result = mysqli_query($conn, $sql);

			if (mysqli_num_rows($result) > 0){

				while ($row = mysqli_fetch_assoc($result)) {
					$row_array['matakuliah'] = $row['matakuliah'];
					$row_array['jam_mulai'] = substr($row['jam_mulai'], 0, 5);
					$row_array['hari'] = $row['hari'];
					
				}
			} else {
				$row_array['matakuliah'] = "";
				$row_array['jam_mulai'] = "";
				$row_array['hari'] = "";
			}
		}
		//end of get matakuliah selanjutnya

		//get pengumuman kelas terbaru
		$sql = "SELECT pengumuman.judul, user.nama
				FROM pengumuman
				INNER JOIN user ON pengumuman.pengirim=user.id_user
				INNER JOIN kelas ON pengumuman.target=kelas.id_kelas
				WHERE kelas.kelas='$kelas'
				ORDER BY upload_date DESC
				LIMIT 1";
		$result = mysqli_query($conn, $sql);

		if (mysqli_num_rows($result) > 0){

			while ($row = mysqli_fetch_assoc($result)) {
				$row_array['judul'] = $row['judul'];
				$row_array['nama'] = $row['nama'];
				
			}
		}else{

			$row_array['judul'] = "";
			$row_array['nama'] = "";
			
		}
		//end of get pengumuman kelas terbaru
		
		//save to array
		array_push($json,$row_array);
		//clear array for next use
		$row_array = array();

		//get jadwal hari ini
		$sql = "SELECT matakuliah, jam_mulai, jam_selesai, nama, hari, foto, ruang.ruang
				FROM jadwal
				INNER JOIN kelas ON jadwal.id_kelas = kelas.id_kelas
				INNER JOIN user ON jadwal.pengampu = user.id_user
				INNER JOIN ruang ON jadwal.ruang = ruang.id_ruang
				WHERE kelas.kelas = '$kelas' AND jadwal.hari = '$id_hari' ORDER BY hari ASC, jam_mulai ASC";
		$result = mysqli_query($conn, $sql);

		if (mysqli_num_rows($result) > 0){

			$hari = array("", "Senin", "Selasa", "Rabu", "Kamis", "Jumat");
			while ($row = mysqli_fetch_assoc($result)) {

				$row_array['matakuliah'] = $row['matakuliah'];
				$row_array['pengampu'] = $row['nama'];
				$row_array['hari'] = $hari[$row['hari']];
				$date_formated = date_create($row['jam_mulai']);
				$row_array['jam_mulai'] = date_format($date_formated,"H:i");
				$date_formated = date_create($row['jam_selesai']);
				$row_array['jam_selesai'] = date_format($date_formated,"H:i");
				$row_array['foto'] = $DIR['USER_IMAGE'].$row['foto'];
				$row_array['ruang'] = $row['ruang'];
				array_push($json2,$row_array);
			}
		}else{

				$row_array['matakuliah'] = "";
				$row_array['pengampu'] = "";
				$row_array['hari'] = "";
				$row_array['jam_mulai'] = "";
				$row_array['jam_selesai'] = "";
				$row_array['foto'] = "";
				$row_array['ruang'] = "";
				array_push($json2,$row_array);
		}
		//end of get jadwal hari ini
		
		////clear array for next use
		$row_array = array();
		
		//get pengumuman terbaru
		$sql = "SELECT pengumuman.pengumuman, pengumuman.judul, pengumuman.target, user.nama, user.foto, pengumuman.upload_date
				FROM pengumuman
				INNER JOIN user ON pengumuman.pengirim=user.id_user
				WHERE target = 99
				ORDER BY upload_date DESC
				LIMIT 3";
		$result = mysqli_query($conn, $sql);

		if (mysqli_num_rows($result) > 0){

			while ($row = mysqli_fetch_assoc($result)) {

				$row_array['pengumuman'] = $row['pengumuman'];
				$row_array['judul'] = $row['judul'];
				$row_array['target'] = $row['target'];
				$row_array['nama'] = $row['nama'];
				$row_array['foto'] = $DIR['USER_IMAGE'].$row['foto'];
				$date_formated = date_create($row['upload_date']);
				$row_array['upload_date'] = date_format($date_formated,"H:i  d F Y");
				array_push($json3,$row_array);
			}
		}else{

				$row_array['pengumuman'] = "";
				$row_array['judul'] = "";
				$row_array['target'] = "";
				$row_array['nama'] = "";
				$row_array['foto'] = "";
				$row_array['upload_date'] = "";
				array_push($json3,$row_array);
		}
		//end of get pengumuman terbaru
		
	} else {

		$json['success'] = 0;
		$json['message'] = 'Authentication Token Mismatch';
	}

	echo json_encode(array('data' => $json, 'jadwal' => $json2, 'pengumuman' => $json3));
	
}
?>
<?php
include 'config.php';
include 'authentication.php';
include 'firebase.php';

//check if request not empty
if (!empty($_SERVER['HTTP_AUTH']) && !empty($_POST['id_surat']) && !empty($_POST['pengusul']) && !empty($_POST['pengganti']) && !empty($_POST['jadwal']) && !empty($_POST['alasan']) && !empty($_POST['keterangan'])) {

	//get request value
	$header = $_SERVER['HTTP_AUTH'];
	$id_surat = $_POST['id_surat'];
	$pengusul = $_POST['pengusul'];
	$pengganti = $_POST['pengganti'];
	$jadwal = $_POST['jadwal'];
	$alasan = $_POST['alasan'];
	$keterangan = $_POST['keterangan'];
	$status = $_POST['status'];

	//check auth token
	if (check_auth($header, $AUTH['TOKEN'])) {

		$sql = "UPDATE surat_tidak_mengajar
				SET pengusul='$pengusul', pengganti='$pengganti', jadwal='$jadwal', alasan='$alasan', keterangan='$keterangan', status='$status', tanggal=CURRENT_TIMESTAMP
				WHERE id_surat='$id_surat'";
		$result = mysqli_query($conn, $sql);

		if (mysqli_query($conn, $sql)) {

			$sql = "SELECT token FROM firebase WHERE id_user='$pengganti' LIMIT 1";
			$result = mysqli_query($conn, $sql);
			$row = mysqli_fetch_assoc($result);
			$key = $AUTH['FIREBASE_API_KEY'];
			$targetNotifikasi = $row['token'];
			$title = "Surat Tidak Bisa Mengajar";
			$judul_stbm = "Permintaan Konfirmasi Dosen Pengganti";
			$isi_stbm = "";
			sendNotification($key, $targetNotifikasi, "", $title, $judul_stbm, $isi_stbm);

			$json['success'] = 1;
			$json['message'] = 'Surat Berhasil Di Update';
		} else {

		    $json['success'] = 0;
			$json['message'] = 'Surat Gagal Di Update, Mohon Coba Lagi';
		}
	} else {

		$json['success'] = 0;
		$json['message'] = 'Authentication Token Mismatch';
	}

	echo json_encode($json);
	
}
?>
<?php
include 'config.php';
include 'authentication.php';

//check if request not empty
if (!empty($_SERVER['HTTP_AUTH']) && !empty($_GET['id_user'])) {

	//get request value
	$header = $_SERVER['HTTP_AUTH'];
	$id_user = $_GET['id_user'];

	//check auth token
	if (check_auth($header, $AUTH['TOKEN'])) {

		$sql = "SELECT a.id_surat, b.nama AS pengusul, c.nama AS pengganti, d.matakuliah, d.jam_mulai, a.alasan, a.keterangan, e.kelas, a.status, f.hari, d.id_jadwal
				FROM surat_tidak_mengajar AS a
				INNER JOIN user AS b ON a.pengusul = b.id_user
				INNER JOIN user AS c ON a.pengganti = c.id_user
				INNER JOIN jadwal AS d ON a.jadwal = d.id_jadwal
				INNER JOIN kelas AS e ON d.id_kelas = e.id_kelas
				INNER JOIN hari AS f ON d.hari = f.id_hari
				WHERE pengusul='$id_user'
				ORDER BY tanggal DESC";
		$result = mysqli_query($conn, $sql);

		$json = array();
		if (mysqli_num_rows($result) > 0){

			while ($row = mysqli_fetch_assoc($result)) {

				$row_array['id_surat'] = $row['id_surat'];
				$row_array['pengusul'] = $row['pengusul'];
				$row_array['pengganti'] = $row['pengganti'];
				$row_array['id_jadwal'] = $row['id_jadwal'];
				$row_array['matakuliah'] = $row['matakuliah'];
				$row_array['hari'] = $row['hari'];
				$row_array['jam_mulai'] = substr($row['jam_mulai'], 0, 5);
				$row_array['kelas'] = $row['kelas'];
				$row_array['alasan'] = $row['alasan'];
				$row_array['keterangan'] = $row['keterangan'];
				$row_array['status'] = $row['status'];
				array_push($json, $row_array);
			}
		} else {

			$row_array['id_surat'] = "";
			$row_array['pengusul'] = "";
			$row_array['pengganti'] = "";
			$row_array['id_jadwal'] = "";
			$row_array['matakuliah'] = "";
			$row_array['hari'] = "";
			$row_array['jam_mulai'] = "";
			$row_array['kelas'] = "";
			$row_array['alasan'] = "";
			$row_array['keterangan'] = "";
			$row_array['status'] = "";
			array_push($json, $row_array);
		}
	} else {

		$json['success'] = 0;
		$json['message'] = 'Authentication Token Mismatch';
	}

	echo json_encode(array('data' => $json));
	
}
?>
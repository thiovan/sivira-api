<?php
include 'config.php';
include 'authentication.php';

//check if request not empty
if (!empty($_SERVER['HTTP_AUTH']) && !empty($_GET['nomor_induk'])) {

	//get request value
	$header = $_SERVER['HTTP_AUTH'];
	$nomor_induk = $_GET['nomor_induk'];

	//check auth token
	if (check_auth($header, $AUTH['TOKEN'])) {

		$sql = "SELECT * FROM user WHERE nomor_induk='$nomor_induk'";
		$result = mysqli_query($conn, $sql);

		if (mysqli_num_rows($result) > 0){

			$json = array();
			while ($row = mysqli_fetch_assoc($result)) {
				$row_array['nama'] = $row['nama'];
				$row_array['no_hp'] = $row['no_hp'];
				$row_array['foto'] = $DIR['USER_IMAGE'].$row['foto'];
				$row_array['last_sync'] = $row['last_sync'];
				array_push($json,$row_array);
			}
		}else{

			$json['success'] = 0;
			$json['message'] = 'User Tidak Ditemukan';
		}
	} else {

		$json['success'] = 0;
		$json['message'] = 'Authentication Token Mismatch';
	}

	echo json_encode($json);
	
}
?>
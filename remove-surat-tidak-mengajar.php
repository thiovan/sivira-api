<?php
include 'config.php';
include 'authentication.php';

//check if request not empty
if (!empty($_SERVER['HTTP_AUTH']) && !empty($_POST['id_surat'])) {

	//get request value
	$header = $_SERVER['HTTP_AUTH'];
	$id_surat = $_POST['id_surat'];

	//check auth token
	if (check_auth($header, $AUTH['TOKEN'])) {

		$sql = "DELETE FROM surat_tidak_mengajar WHERE id_surat='$id_surat'";
		$result = mysqli_query($conn, $sql);

		if (mysqli_query($conn, $sql)) {

			$json['success'] = 1;
			$json['message'] = 'Surat Berhasil Di Hapus';
		} else {

		    $json['success'] = 0;
			$json['message'] = 'Surat Gagal Di Hapus, Mohon Coba Lagi';
		}
	} else {

		$json['success'] = 0;
		$json['message'] = 'Authentication Token Mismatch';
	}

	echo json_encode($json);
	
}
?>
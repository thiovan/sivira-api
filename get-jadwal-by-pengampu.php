<?php
include 'config.php';
include 'authentication.php';

//check if request not empty
if (!empty($_SERVER['HTTP_AUTH']) && !empty($_GET['id_user'])) {

	//get request value
	$header = $_SERVER['HTTP_AUTH'];
	$id_user = $_GET['id_user'];

	//check auth token
	if (check_auth($header, $AUTH['TOKEN'])) {

		$sql = "SELECT jadwal.id_jadwal, matakuliah, kelas.kelas, jam_mulai, jam_selesai, nama, hari, foto, ruang.ruang
				FROM jadwal
				INNER JOIN kelas ON jadwal.id_kelas = kelas.id_kelas
				INNER JOIN user ON jadwal.pengampu = user.id_user
				INNER JOIN ruang ON jadwal.ruang = ruang.id_ruang
				WHERE jadwal.pengampu = '$id_user' AND jadwal.status='1' ORDER BY hari ASC, jam_mulai ASC";
		$result = mysqli_query($conn, $sql);

		if (mysqli_num_rows($result) > 0){

			$json = array();
			$hari = array("", "Senin", "Selasa", "Rabu", "Kamis", "Jumat");
			while ($row = mysqli_fetch_assoc($result)) {
				$row_array['id_jadwal'] = $row['id_jadwal'];
				$row_array['matakuliah'] = $row['matakuliah'];
				$row_array['kelas'] = $row['kelas'];
				$row_array['pengampu'] = $row['nama'];
				$row_array['hari'] = $hari[$row['hari']];
				$date_formated = date_create($row['jam_mulai']);
				$row_array['jam_mulai'] = date_format($date_formated,"H:i");
				$date_formated = date_create($row['jam_selesai']);
				$row_array['jam_selesai'] = date_format($date_formated,"H:i");
				$row_array['foto'] = $DIR['USER_IMAGE'].$row['foto'];
				$row_array['ruang'] = $row['ruang'];
				array_push($json,$row_array);
			}
		}else{

			$json['success'] = 0;
			$json['message'] = 'Jadwal Tidak Ditemukan';
		}
	} else {

		$json['success'] = 0;
		$json['message'] = 'Authentication Token Mismatch';
	}

	echo json_encode(array('data' => $json));
	
}
?>
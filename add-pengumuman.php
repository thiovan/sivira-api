<?php
include 'config.php';
include 'authentication.php';
include 'firebase.php';

//check if request not empty
if (!empty($_SERVER['HTTP_AUTH']) && !empty($_POST['judul']) && !empty($_POST['pengumuman']) && !empty($_POST['target']) && !empty($_POST['pengirim'])) {

	//get request value
	$header = $_SERVER['HTTP_AUTH'];
	$judul = $_POST['judul'];
	$pengumuman = $_POST['pengumuman'];
	$target = $_POST['target'];
	$pengirim = $_POST['pengirim'];

	//check auth token
	if (check_auth($header, $AUTH['TOKEN'])) {
		
		$sql = "INSERT INTO pengumuman VALUES ('', '$judul', '$pengumuman', '$target', '$pengirim', CURRENT_TIMESTAMP)";
		if (mysqli_query($conn, $sql)) {

			$key = $AUTH['FIREBASE_API_KEY'];
			if ($target != "99") {
				$kelas = array('', 'IK-1A', 'IK-1B', 'IK-2A', 'IK-2B', 'IK-3A', 'IK-3B');

				$targetNotifikasi = $kelas[$target];
				$title = "Pengumuman Baru (" . $kelas[$target] . ")";
				$judul_pengumuman = $judul;
				$isi_pengumuman = $pengumuman;
				sendNotification($key, "", $targetNotifikasi, $title, $judul_pengumuman, $isi_pengumuman);
			} else {

				$targetNotifikasi = "global";
				$title = "Pengumuman Baru";
				$judul_pengumuman = $judul;
				$isi_pengumuman = $pengumuman;
				sendNotification($key, "", $targetNotifikasi, $title, $judul_pengumuman, $isi_pengumuman);
			}
			$json['success'] = 1;
			$json['message'] = 'Pengumuman Berhasil Dikirim';
		} else {

			$json['success'] = 0;
			$json['message'] = 'Pengumuman Gagal Dikirim, Mohon Coba Lagi';
		}
	} else {

		$json['success'] = 0;
		$json['message'] = 'Authentication Token Mismatch';
	}

	echo json_encode($json);
	
}
?>
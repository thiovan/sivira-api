<?php
include 'config.php';
include 'authentication.php';

//check if request not empty
if (!empty($_SERVER['HTTP_AUTH']) && !empty($_GET['id_user'])) {

	//get request value
	$header = $_SERVER['HTTP_AUTH'];
	$id_user = $_GET['id_user'];
	
	$arr_id_hari = array(
				"Monday" => 1,
				"Tuesday" => 2,
				"Wednesday" => 3,
				"Thursday" => 4,
				"Friday" => 5,
				"Saturday" => 6,
				"Sunday" => 7
		);
	$id_hari = $arr_id_hari[date('l')];

	//check auth token
	if (check_auth($header, $AUTH['TOKEN'])) {

		$json = array();
		$json2 = array();
		$json3 = array();

		//get jadwal mengajar selanjutnya
		$sql = "SELECT matakuliah, jam_mulai, hari.hari
				FROM jadwal
				INNER JOIN kelas ON jadwal.id_kelas = kelas.id_kelas
				INNER JOIN hari ON jadwal.hari = hari.id_hari
				WHERE jadwal.pengampu = '$id_user' AND jadwal.hari = '$id_hari' AND jam_mulai > CURRENT_TIME
				LIMIT 1";
		$result = mysqli_query($conn, $sql);

		if (mysqli_num_rows($result) > 0){

			while ($row = mysqli_fetch_assoc($result)) {

				$row_array['matakuliah'] = $row['matakuliah'];
				$row_array['jam_mulai'] = substr($row['jam_mulai'], 0, 5);
				$row_array['hari'] = $row['hari'];
			}
		}else{

			if ($id_hari == "6" || $id_hari == "7") {

				$next_id_hari = 1;
			} else {

				$next_id_hari = $id_hari + 1;
			}
			
			while (true) {

				if ($next_id_hari == "6" || $next_id_hari == "7") {

					$next_id_hari = 1;
					continue;
				} else {

					$sql = "SELECT matakuliah, jam_mulai, hari.hari
					FROM jadwal
					INNER JOIN kelas ON jadwal.id_kelas = kelas.id_kelas
					INNER JOIN hari ON jadwal.hari = hari.id_hari
					WHERE jadwal.pengampu = '$id_user' AND jadwal.hari = '$next_id_hari'
					LIMIT 1";
					$result = mysqli_query($conn, $sql);

					if (mysqli_num_rows($result) > 0) {

						while ($row = mysqli_fetch_assoc($result)) {

							$row_array['matakuliah'] = $row['matakuliah'];
							$row_array['jam_mulai'] = substr($row['jam_mulai'], 0, 5);
							$row_array['hari'] = $row['hari'];
						}
						break;
					}
					$next_id_hari = $next_id_hari + 1;
				}

			}

		}
		//end of get jadwal mengajar selanjutnya
		
		array_push($json,$row_array);
		//clear array for next use
		$row_array = array();

		//get surat masuk terbaru
		$sql = "SELECT * FROM surat WHERE id_user='$id_user' LIMIT 3";
		$result = mysqli_query($conn, $sql);

		if (mysqli_num_rows($result) > 0){

			while ($row = mysqli_fetch_assoc($result)) {
				$row_array['jenis'] = $row['jenis'];
				$row_array['judul'] = $row['judul'];
				if (empty($row['foto'])) {
					
					$row_array['foto'] = $row['foto'];
				} else {

					$foto = "";
					$foto_array = explode(";", $row['foto']);
					for ($i=0; $i < count($foto_array)-1; $i++) { 
						$foto = $foto . $DIR['SURAT_IMAGE'] . $foto_array[$i] . ";";
					}
					$row_array['foto'] = str_replace(" ", "%20", $foto);
				}
				$date_formated = date_create($row['tanggal']);
				$row_array['tanggal'] = date_format($date_formated,"d M");
				$date_formated = date_create($row['tanggal_surat']);
				$row_array['tanggal_surat'] = date_format($date_formated,"H:i  d F Y");
				array_push($json2,$row_array);
			}
		}else{

				$row_array['jenis'] = "";
				$row_array['judul'] = "";
				$row_array['foto'] = "";
				$row_array['tanggal'] = "";
				$row_array['tanggal_surat'] = "";
				array_push($json2,$row_array);
			
		}
		//end of get surat masuk terbaru
		
		//clear array for next use
		$row_array = array();
		
		//get pengumuman terbaru
		$sql = "SELECT pengumuman.pengumuman, pengumuman.judul, pengumuman.target, user.nama, user.foto, pengumuman.upload_date
				FROM pengumuman
				INNER JOIN user ON pengumuman.pengirim=user.id_user
				WHERE target = 99
				ORDER BY upload_date DESC
				LIMIT 3";
		$result = mysqli_query($conn, $sql);

		if (mysqli_num_rows($result) > 0){

			while ($row = mysqli_fetch_assoc($result)) {

				$row_array['pengumuman'] = $row['pengumuman'];
				$row_array['judul'] = $row['judul'];
				$row_array['target'] = $row['target'];
				$row_array['nama'] = $row['nama'];
				$row_array['foto'] = $DIR['USER_IMAGE'].$row['foto'];
				$date_formated = date_create($row['upload_date']);
				$row_array['upload_date'] = date_format($date_formated,"H:i  d F Y");
				array_push($json3,$row_array);
			}
		}else{

				$row_array['pengumuman'] = "";
				$row_array['judul'] = "";
				$row_array['target'] = "";
				$row_array['nama'] = "";
				$row_array['foto'] = "";
				$row_array['upload_date'] = "";
				array_push($json3,$row_array);
		}
		//end of get pengumuman terbaru
		
	} else {

		$json['success'] = 0;
		$json['message'] = 'Authentication Token Mismatch';
	}

	echo json_encode(array('data' => $json, 'surat' => $json2, 'pengumuman' => $json3));
	
}
?>
<?php
include 'config.php';
include 'authentication.php';

//check if request not empty
if (!empty($_SERVER['HTTP_AUTH'])) {

	//get request value
	$header = $_SERVER['HTTP_AUTH'];

	//check auth token
	if (check_auth($header, $AUTH['TOKEN'])) {

		$sql = "SELECT user.foto, user.nama, user.nomor_induk, posisi.last_sync
				FROM posisi
				INNER JOIN user ON posisi.id_user = user.id_user
				WHERE user.tipe IN ('Dosen', 'Ketua Program Studi')
				ORDER BY posisi.last_sync DESC";
		$result = mysqli_query($conn, $sql);

		if (mysqli_num_rows($result) > 0){

			$json = array();
			while ($row = mysqli_fetch_assoc($result)) {
				$row_array['foto'] = $DIR['USER_IMAGE'].$row['foto'];
				$row_array['nama'] = $row['nama'];
				$row_array['nomor_induk'] = $row['nomor_induk'];
				$row_array['last_sync'] = strtotime($row['last_sync']) * 1000;
				array_push($json,$row_array);
			}
		}else{

			$json['success'] = 0;
			$json['message'] = 'Posisi Tidak Ditemukan';
		}
	} else {

		$json['success'] = 0;
		$json['message'] = 'Authentication Token Mismatch';
	}

	echo json_encode(array('data' => $json));
	
}
?>
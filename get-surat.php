<?php
include 'config.php';
include 'authentication.php';

//check if request not empty
if (!empty($_SERVER['HTTP_AUTH']) && !empty($_GET['id_user'])) {

	//get request value
	$header = $_SERVER['HTTP_AUTH'];
	$id_user = $_GET['id_user'];

	//check auth token
	if (check_auth($header, $AUTH['TOKEN'])) {

		$sql = "SELECT * FROM surat WHERE id_user='$id_user'";
		$result = mysqli_query($conn, $sql);

		$json = array();
		if (mysqli_num_rows($result) > 0){

			while ($row = mysqli_fetch_assoc($result)) {
				$row_array['jenis'] = $row['jenis'];
				$row_array['judul'] = $row['judul'];
				if (empty($row['foto'])) {

					$row_array['foto'] = $row['foto'];
				} else {

					$foto = "";
					$foto_array = explode(";", $row['foto']);
					for ($i=0; $i < count($foto_array)-1; $i++) { 
						$foto = $foto . $DIR['SURAT_IMAGE'] . $foto_array[$i] . ";";
					}
					$row_array['foto'] = str_replace(" ", "%20", $foto);
				}
				$date_formated = date_create($row['tanggal']);
				$row_array['tanggal'] = date_format($date_formated,"d M");
				$date_formated = date_create($row['tanggal_surat']);
				$row_array['tanggal_surat'] = date_format($date_formated,"H:i  d F Y");
				array_push($json,$row_array);
			}
		}else{

			$row_array['jenis'] = "";
			$row_array['judul'] = "";
			$row_array['foto'] = "";
			$row_array['tanggal'] = "";
			$row_array['tanggal_surat'] = "";
			array_push($json,$row_array);
		}
	} else {

		$json['success'] = 0;
		$json['message'] = 'Authentication Token Mismatch';
	}

	echo json_encode(array('data' => $json));
	
}
?>
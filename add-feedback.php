<?php
include 'config.php';
include 'authentication.php';

//check if request not empty
if (!empty($_SERVER['HTTP_AUTH']) && !empty($_POST['pengirim']) && !empty($_POST['feedback'])) {

	//get request value
	$header = $_SERVER['HTTP_AUTH'];
	$pengirim = $_POST['pengirim'];
	$feedback = $_POST['feedback'];

	//check auth token
	if (check_auth($header, $AUTH['TOKEN'])) {
		
		$sql = "INSERT INTO feedback VALUES ('', '$pengirim', '$feedback', CURRENT_TIMESTAMP)";
		if (mysqli_query($conn, $sql)) {

		    $json['success'] = 1;
			$json['message'] = 'Feedback Berhasil Dikirim';
		} else {

		    $json['success'] = 0;
			$json['message'] = 'Feedback Gagal Dikirim, Mohon Coba Lagi';
		}
	} else {

		$json['success'] = 0;
		$json['message'] = 'Authentication Token Mismatch';
	}

	echo json_encode($json);
	
}
?>
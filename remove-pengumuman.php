<?php
include 'config.php';
include 'authentication.php';

//check if request not empty
if (!empty($_SERVER['HTTP_AUTH']) && !empty($_POST['id_pengumuman'])) {

	//get request value
	$header = $_SERVER['HTTP_AUTH'];
	$id_pengumuman = $_POST['id_pengumuman'];

	//check auth token
	if (check_auth($header, $AUTH['TOKEN'])) {

		$sql = "DELETE FROM pengumuman WHERE id_pengumuman='$id_pengumuman'";
		$result = mysqli_query($conn, $sql);

		if (mysqli_query($conn, $sql)) {

			$json['success'] = 1;
			$json['message'] = 'Pengumuman Berhasil Di Hapus';
		} else {

		    $json['success'] = 0;
			$json['message'] = 'Pengumuman Gagal Di Hapus, Mohon Coba Lagi';
		}
	} else {

		$json['success'] = 0;
		$json['message'] = 'Authentication Token Mismatch';
	}

	echo json_encode($json);
	
}
?>
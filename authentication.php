<?php
//Create Connection
$conn = mysqli_connect($DB['HOSTNAME'], $DB['USERNAME'] , $DB['PASSWORD'], $DB['DATABASE']);
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

//Check Authtentication Token
function check_auth($header, $token){
	if ($header === $token) {
		return true;
	}
	return false;
}

?>
<?php
//Set JSON response content type
header('Content-Type: application/json');

// Turn off error reporting
// For Debugging Please Disable This Setting
error_reporting(0);

//Array To Store DB Config, AUTH Token, and Upload Directory
$DB = array();
$AUTH = array();
$DIR = array();

//Set Timezone
date_default_timezone_set('Asia/Jakarta');

//Config Database
$DB['HOSTNAME'] = "localhost";
$DB['USERNAME'] = "root";
$DB['PASSWORD'] = "";
$DB['DATABASE'] = "sivira";

//Config Auth Token
$AUTH['TOKEN'] = "PHf9uu6Uxr6idXSKWlyt";

//Firebase API Key
$AUTH['FIREBASE_API_KEY'] = "AAAAxfrLXq8:APA91bGRZMnKeXpI45DzFwrmTq1s4GxPNVBnZE9RIFQaavCORcwa6GlfmdNpG6ZOvdv_57DAdHEMx_H7P2QnBPpCvySyClxQPEMDhdr1K9mGX6gTtRqCIciJgtWpSkWDQzsr65k7tGtU";

//Server Base URL
$DIR['BASE_URL'] = "http://192.168.0.3/web/sivira/";

//User Image Directory
$DIR['USER_IMAGE'] = $DIR['BASE_URL'] . "user-image/";

//Surat Image Directory
$DIR['SURAT_IMAGE'] = $DIR['BASE_URL'] . "surat-image/";

//Apps Latest Version
$APP['LATEST_VERSION'] = "0.0.2-beta";
$APP['LATEST_VERSION_CODE'] = 2;
$APP['URL'] = "https://play.google.com/store/apps/details?id=polines.sivira";
$APP['CHANGELOG'] = array(
							"-Changelog 1",
							"-Changelog 2"
						 );
?>
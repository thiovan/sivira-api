<?php
include 'config.php';
include 'authentication.php';

//check if request not empty
if (!empty($_SERVER['HTTP_AUTH']) && !empty($_POST['id_ruang']) && !empty($_POST['id_user'])) {

	//get request value
	$header = $_SERVER['HTTP_AUTH'];
	$id_ruang = $_POST['id_ruang'];
	$id_user = $_POST['id_user'];

	//check auth token
	if (check_auth($header, $AUTH['TOKEN'])) {

		$sql = "SELECT * FROM posisi WHERE id_user='$id_user'";
		$result = mysqli_query($conn, $sql);
		if (mysqli_num_rows($result) > 0) {

			$sql = "UPDATE posisi SET id_ruang='$id_ruang', id_user='$id_user', last_sync=CURRENT_TIMESTAMP WHERE id_user='$id_user'";
			$result = mysqli_query($conn, $sql);
			if (mysqli_query($conn, $sql)) {

				$json['success'] = 1;
				$json['message'] = 'Data Berhasil Di Update';
			} else {

			    $json['success'] = 0;
				$json['message'] = 'Data Gagal Di Update, Mohon Coba Lagi';
			}
		} else {

			$sql = "INSERT INTO posisi (id_posisi, id_ruang, id_user, last_sync) VALUES ('', '$id_ruang', '$id_user', CURRENT_TIMESTAMP)";
			if (mysqli_query($conn, $sql)) {

			    $json['success'] = 1;
				$json['message'] = 'Data Berhasil Di Update';
			} else {

			    $json['success'] = 0;
				$json['message'] = 'Data Gagal Di Update, Mohon Coba Lagi';
			}
		}
	} else {

		$json['success'] = 0;
		$json['message'] = 'Authentication Token Mismatch';
	}

	echo json_encode($json);
	
}
?>